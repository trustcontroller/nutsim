package com.portifacto.nutsim.playbook;

import com.portifacto.nutsim.parameters.GraphType;
import com.portifacto.nutsim.parameters.Parameters;
import com.portifacto.nutsim.parameters.Role;
import com.portifacto.nutsim.parameters.RoleLabel;
import org.testng.annotations.Test;

import static org.testng.Assert.*;

public class PlaybookTest {

    @Test
    public void simpleParameters() throws PlaybookException{

        Parameters parameters=new Parameters().setSize(3).setSteps(2).setRuns(1).setGraphType(GraphType.RANDOM).setDensity(1)
                .addRole(new Role().setLabel(RoleLabel.NORMAL).setMean(0.8));
        Playbook playbook=Playbook.fromParameters(parameters);
        System.out.println(playbook.toString());
    }

    @Test
    public void twoRoles() throws PlaybookException{

        Parameters parameters=new Parameters()
                .setSize(3)
                .setSteps(2)
                .setRuns(1)
                .setGraphType(GraphType.RANDOM)
                .setDensity(1)
                .addRole(new Role()
                        .setLabel(RoleLabel.NORMAL)
                        .setMean(0.8)
                        .setFraction(2))
                .addRole((new Role())
                        .setLabel(RoleLabel.TRUSTWORTHY)
                        .setMean(1.0)
                        .setFraction(1));
        Playbook playbook=Playbook.fromParameters(parameters);
        System.out.println(playbook.toString());
    }


}
package com.portifacto.nutsim.common;

import org.junit.Assert;
import org.testng.annotations.Test;

import static org.testng.Assert.*;

public class BeliefTest {

    private static double DELTA=0.001;

    @Test
    public void validityOfConstants() {
        Belief belief1=Belief.BELIEF;
        Belief belief2=Belief.DISBELIEF;
        Belief belief3=Belief.UNCERTAINTY;

        Assert.assertTrue(belief1.isValid());
        Assert.assertTrue(belief2.isValid());
        Assert.assertTrue(belief3.isValid());
    }

    @Test
    public void compatibleOfEUAndExpectation() {
        Belief b1=Belief.ofEU(0,0);
        Assert.assertEquals(0,b1.getExpectation(),DELTA);

        Belief b2=Belief.ofEU(1,0);
        Assert.assertEquals(1,b2.getExpectation(),DELTA);

        Belief b3=Belief.ofEU(0,1);
        Assert.assertEquals(0,b3.getExpectation(),DELTA);

        Belief b4=Belief.ofEU(1,1);
        Assert.assertEquals(1,b4.getExpectation(),DELTA);

        Belief b5=Belief.ofEU(0.5,0);
        Assert.assertEquals(0.5,b5.getExpectation(),DELTA);

        Belief b6=Belief.ofEU(0.5,1);
        Assert.assertEquals(0.5,b6.getExpectation(),DELTA);

        Belief b7=Belief.ofEU(0.5,0.5);
        Assert.assertEquals(0.5,b7.getExpectation(),DELTA);

        Belief b8=Belief.ofEU(0.7,0.8);
        Assert.assertEquals(0.7,b8.getExpectation(),DELTA);

    }

    @Test
    public void decayTest() {
        Belief b1=Belief.of(0.3,0.5,0.2);
        System.out.println(b1);
        Belief b2=b1.decay(0.4);
        System.out.println(b2);
        Belief b3=b2.decay(0.4);
        System.out.println(b3);
    }

    @Test
    public void commutableConsensus() {
        Belief b1=Belief.of(0.5,0.2,0.3);
        Belief b2=Belief.of(0.3,0.2,0.5);
        Belief b3=Belief.of(0.2,0.5,0.3);
        Belief b4=Belief.of(0.5,0.3,0.2);

        System.out.println(b1.consensus(b2).consensus(b3).consensus(b4));
        System.out.println(b4.consensus(b3).consensus(b2).consensus(b1));

        System.out.println(Belief.BELIEF.consensus(Belief.DISBELIEF));
        System.out.println(Belief.DISBELIEF.consensus(Belief.BELIEF));

        System.out.println(Belief.BELIEF.consensus(Belief.UNCERTAINTY));
        System.out.println(Belief.UNCERTAINTY.consensus(Belief.BELIEF));

        System.out.println(Belief.UNCERTAINTY.consensus(b1).consensus(b2));
        System.out.println(b1.consensus(b2).consensus(Belief.UNCERTAINTY));

    }

    @Test
    public void compatibilityOfEU() {

        System.out.println(Belief.ofEU(0,0));
        System.out.println(Belief.ofEU(1,0));
        System.out.println(Belief.ofEU(0,1));
        System.out.println(Belief.ofEU(1,1));
        System.out.println(Belief.ofEU(0.5,0));
        System.out.println(Belief.ofEU(0.5,1));
        System.out.println(Belief.ofEU(0.5,0.5));


    }

    @Test
    public void discountingSensibility() {
        System.out.println(Belief.BELIEF.discounting(Belief.of(0.01,0.99,0.0)));
        System.out.println(Belief.of(0.5,0.3,0.2).discounting(Belief.of(0.01,0.99,0.0)));

        Belief calculated=Belief.of(0.01,1-0.01,0,0.01); // certain in myself
        Belief opinion=calculated.discounting(Belief.BELIEF);

        System.out.println(opinion);
        System.out.println(Belief.BELIEF.discounting(calculated));

    }

}
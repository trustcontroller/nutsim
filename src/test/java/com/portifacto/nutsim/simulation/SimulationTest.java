package com.portifacto.nutsim.simulation;

import com.portifacto.nutsim.analytics.Analytics;
import com.portifacto.nutsim.parameters.*;
import com.portifacto.nutsim.playbook.Playbook;
import com.portifacto.nutsim.playbook.PlaybookException;
import com.portifacto.nutsim.report.Report;
import org.testng.annotations.Test;

import java.util.ArrayList;
import java.util.List;

public class SimulationTest {

    @Test
    public void simpleRun() throws PlaybookException,SimulationException {
        Parameters parameters=new Parameters()
                .setSize(3)
                .setSteps(3)
                .setRuns(1)
                .setGraphType(GraphType.RANDOM)
                .setDensity(1)
                .addRole(new Role()
                        .setLabel(RoleLabel.NORMAL)
                        .setMean(0.8)
                        .setFraction(2))
                .addRole((new Role())
                        .setLabel(RoleLabel.TRUSTWORTHY)
                        .setMean(1.0)
                        .setFraction(1))
                .setCalculation(new Calculation()
                        .setType(CalculationType.EXPONENTIAL)
                        .setLambda(2.4))
                ;
        Playbook playbook=Playbook.fromParameters(parameters);
        ISimulation simulation=new Simulation();
        Report report= simulation.run(playbook);
        System.out.println(playbook.toString());
        System.out.println(report.toString());

    }

    @Test
    public void sigmoid() throws PlaybookException, SimulationException {

        List<Double> points=new ArrayList<>();


        for (int good = 0; good <= 100; good+=10) { // percentage

            int bad=100-good;

            System.out.println("good: " + good +
                    "%, bad: " + bad + "%");

            // run experiment
            Parameters parameters = new Parameters()
                    .setTitle(this.getClass().getSimpleName())
                    .setRuns(4)
                    .setSteps(20)
                    .setDensity(3)
                    .addRole(new Role()
                            .setLabel(RoleLabel.TRUSTWORTHY)
                            .setMean(1.0)
                            .setDeviation(0.0)
                            .setFraction(good)
                            .setTrustworthiness(new Trustworthiness()
                                    .setValue(0.5)
                                    .setRecalculate(true))
                            .setConfidence(new Confidence()
                                    .setValue(1.0)
                                    .setRecalculate(false))
                    )
                    .addRole(new Role()
                            .setLabel(RoleLabel.FAULTY)
                            .setMean(0.0)
                            .setFraction(bad)
                            .setTrustworthiness(new Trustworthiness()
                                    .setValue(0.5)
                                    .setRecalculate(true))
                            .setConfidence(new Confidence()
                                    .setValue(1.0)
                                    .setRecalculate(false))
                    )
                    .setCalculation(new Calculation()
                            .setType(CalculationType.EXPONENTIAL)
                            .setLambda(2.4))
                    .setPhenomenon(new Phenomenon()
                            .setValue(1.0))
                    .setGraphType(GraphType.RANDOM)
                    .setDecay(0.8)
                    .setSize(20);

            Playbook playbook = Playbook.fromParameters(parameters);

            Simulation simulation = new Simulation();
            Report report = simulation.run(playbook);

            Analytics analytics = new Analytics(report);
            List<Double> unitResponse = analytics.averageUnitResponsePerStepStandard();
            double lastResponse1 = unitResponse.get(unitResponse.size() - 1);
            points.add(lastResponse1);

        }
        int i=0;
        for(Double d:points) {
            System.out.println("  at "+i+" unit response "+d);
            i++;
        }

    }


}
package com.portifacto.nutsim.experiment;

import java.util.List;

public class ExperimentHelper implements ExperimentCommon {

    public static class Point {
        public final double lastUnitResponse;
        public final double authority;
        public final double faulty;
        public final double good;

        public Point(double lastUnitResponse, double authority, double faulty, double good) {
            this.lastUnitResponse = lastUnitResponse;
            this.authority = authority;
            this.faulty = faulty;
            this.good = good;
        }
    }

    public static class LastUnitResponse {

        final double density;
        final double response;
        final List<Double> averageUnitResponsePerStep;

        LastUnitResponse(double density, double response, List<Double> averageResponsePerStep) {
            this.density = density;
            this.response = response;
            this.averageUnitResponsePerStep = averageResponsePerStep;
        }
    }

    public static class Line {
        public final double reference;
        public final double resilience;
        public final double robustness;
        public final List<Point> points;

        public Line(double reference, double resilience, double robustness, List<Point> points) {
            this.reference = reference;
            this.resilience = resilience;
            this.robustness = robustness;
            this.points = points;
        }
    }

    private class ParameterSet {
        final private double good;
        final private double bad;
        final private double density;
        final private double decay;

        private boolean authority = false;
        private double trustworthiness = 0.5;

        public ParameterSet(double good, double bad, double density, double decay) {
            this.good = good;
            this.bad = bad;
            this.density = density;
            this.decay = decay;
        }

        public double getGood() {
            return good;
        }

        public double getBad() {
            return bad;
        }

        public double getDensity() {
            return density;
        }

        public double getDecay() {
            return decay;
        }

    }

    private class ExperimentResult {
        public ParameterSet parameterSet;
        public List<Double> unitResponse;
        public List<Double> unitResponseBelief;
        public List<Double> f1;
        public List<Double> traceNode0;
        public List<Double> traceBeliefNode0;

        public ExperimentResult(ParameterSet parameterSet, List<Double> unitResponse, List<Double> unitResponseBelief,
                                List<Double> f1, List<Double> traceNode0, List<Double> traceBeliefNode0) {
            this.parameterSet = parameterSet;
            this.unitResponse = unitResponse;
            this.unitResponseBelief = unitResponseBelief;
            this.f1 = f1;
            this.traceNode0 = traceNode0;
            this.traceBeliefNode0 = traceBeliefNode0;
        }
    }


    public double calculateTransitionLoHi(List<Point> points) {
        // first result above or equal 0.5 and next below 0.5
        for (int i = 0; i < points.size(); i++) {
            if (points.get(i).lastUnitResponse == 0.5) return points.get(i).authority + points.get(i).good;
            if (i > 0 && points.get(i - 1).lastUnitResponse > 0.5 && points.get(i).lastUnitResponse < 0.5) {
                // between i-1 and i
                double fromX = points.get(i - 1).authority + points.get(i - 1).good;
                double toX = points.get(i).authority + points.get(i).good;
                double fromY = points.get(i - 1).lastUnitResponse;
                double toY = points.get(i).lastUnitResponse;
                double beforeY = 0.5 - fromY;
                double allY = Math.abs(toY - fromY);
                double crossingX = fromX + (beforeY / allY) * (toX - fromX);
                return 1 - crossingX;
            }
        }
        return 0;
    }

    public double calculateTransitionHiLo(List<Point> points) {
        // first result below or equal 0.5 and next above 0.5
        for (int i = 0; i < points.size(); i++) {
            if (points.get(i).lastUnitResponse == 0.5) return points.get(i).authority + points.get(i).good;
            if (i > 0 && points.get(i - 1).lastUnitResponse < 0.5 && points.get(i).lastUnitResponse > 0.5) {
                // between i-1 and i
                double fromX = points.get(i - 1).authority + points.get(i - 1).good;
                double toX = points.get(i).authority + points.get(i).good;
                double fromY = points.get(i - 1).lastUnitResponse;
                double toY = points.get(i).lastUnitResponse;
                double beforeY = 0.5 - fromY;
                double allY = Math.abs(toY - fromY);
                double crossingX = fromX + (beforeY / allY) * (toX - fromX);
                return 1 - crossingX;
            }
        }
        return 0;
    }

    public double calculateRobustness(List<Point> points, double width) {
        // calculated as the averaged sum of everything above 0.5
        double robustness = points.stream()
                .mapToDouble(x -> x.lastUnitResponse - 0.5)
                .filter(x -> x >= 0)
                .map(x -> x * width)
                .sum();
        return robustness;
    }


}
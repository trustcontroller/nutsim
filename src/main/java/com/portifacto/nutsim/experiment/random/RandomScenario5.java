package com.portifacto.nutsim.experiment.random;


import com.portifacto.nutsim.analytics.Analytics;
import com.portifacto.nutsim.analytics.CsvWriter;
import com.portifacto.nutsim.common.Belief;
import com.portifacto.nutsim.experiment.ExperimentCommon;
import com.portifacto.nutsim.experiment.IExperiment;
import com.portifacto.nutsim.parameters.*;
import com.portifacto.nutsim.playbook.Playbook;
import com.portifacto.nutsim.playbook.PlaybookException;
import com.portifacto.nutsim.report.Report;
import com.portifacto.nutsim.simulation.Simulation;

import java.util.LinkedList;
import java.util.List;

/**
 * Scenario 5: node failure
 */
public class RandomScenario5 implements ExperimentCommon, IExperiment {

    private static final int ALTERNATIVE = 10;

    private static class Point {
        private final double reference;
        private final double normal;
        private final double response;
        private final List<Double> trace;

        public Point(double reference, double normal, double response, List<Double> trace) {
            this.reference = reference;
            this.normal = normal;
            this.response = response;
            this.trace = trace;
        }
    }

    public static void main(String[] argv) throws PlaybookException {
        IExperiment experiment = new RandomScenario5();
        try {
            experiment.process();
        } catch (Exception e) {
            System.out.println("Exception: " + e);
        }
    }

    public void process() throws PlaybookException {

        System.out.println("running " + this.getClass().getSimpleName());

        List<Point> points = new LinkedList<>();

        for (int reference = 0; reference <= 20; reference += 1) { // percentage

            int normal = 100 - reference - ALTERNATIVE;

            if (normal < 0) continue;

            System.out.println("reference: " + reference +
                    "%, normal: " + normal + "%, alternative: " + ALTERNATIVE);

            // run experiment
            Parameters parameters = new Parameters()
                    .setTitle(this.getClass().getSimpleName())
                    .setRuns(RUNS)
                    .setSteps(STEPS)
                    .setDensity(DENSITY)
                    .addRole(new Role()
                            .setLabel(RoleLabel.ALTERNATIVE)
                            .setMean(1.0)
                            .setDeviation(0.0)
                            .setFraction(ALTERNATIVE)
                            .setTrustworthiness(new Trustworthiness()
                                    .setValue(0.5)
                                    .setRecalculate(true))
                            .setConfidence(new Confidence()
                                    .setValue(1.0)
                                    .setRecalculate(false))
                            .setJosang(new Josang()
                                    .setValue(Belief.UNCERTAINTY)
                                    .setRecalculate(true))
                            .setAlternative(new Alternative()
                                    .setStep(STEPS / 2)
                                    .setValue(0.0)
                                    .setDeviation(0.0)
                            )
                    )
                    .addRole(new Role()
                            .setLabel(RoleLabel.REFERENCE)
                            .setMean(1.0)
                            .setDeviation(0.0)
                            .setFraction(reference)
                            .setTrustworthiness(new Trustworthiness()
                                    .setValue(1.0)
                                    .setRecalculate(false))
                            .setConfidence(new Confidence()
                                    .setValue(1.0)
                                    .setRecalculate(false))
                            .setJosang(new Josang()
                                    .setValue(Belief.BELIEF)
                                    .setRecalculate(false))
                    )
                    .addRole(new Role()
                            .setLabel(RoleLabel.NORMAL)
                            .setMean(1.0)
                            .setDeviation(0.0)
                            .setFraction(normal)
                            .setTrustworthiness(new Trustworthiness()
                                    .setValue(0.5)
                                    .setRecalculate(true))
                            .setConfidence(new Confidence()
                                    .setValue(1.0)
                                    .setRecalculate(false))
                            .setJosang(new Josang()
                                    .setValue(Belief.UNCERTAINTY)
                                    .setRecalculate(true))

                    )
                    .setCalculation(new Calculation()
                            .setType(CalculationType.EXPONENTIAL)
                            .setLambda(LAMBDA))
                    .setPhenomenon(new Phenomenon()
                            .setValue(1.0))
                    .setGraphType(GraphType.RANDOM)
                    .setDecay(DECAY)
                    .setFusion(new Fusion()
                            .setType(FusionType.TRANSITIVITY))
                    .setSize(SIZE);

            Playbook playbook = Playbook.fromParameters(parameters);

            Simulation ewmaSimulation = new Simulation();
            Report report = ewmaSimulation.run(playbook);

            Analytics analytics = new Analytics(report);
            List<Double> unitResponse = analytics.averageUnitResponsePerStepStandard();
            List<Double> traceNode0 = analytics.nodeStandardTraceAverage(0);

            double lastResponse = unitResponse.get(unitResponse.size() - 1);

            points.add(new Point(
                    (double) reference / 100.0,
                    (double) normal / 100.0,
                    lastResponse,
                    traceNode0));
        }

        String path = PATH + this.getClass().getSimpleName() + ".csv";

        CsvWriter analyticWriter = new CsvWriter(path);

        analyticWriter.write("parameters");
        analyticWriter.write("", "density", DENSITY);
        analyticWriter.write("", "decay", DECAY);
        analyticWriter.write("", "size", SIZE);
        analyticWriter.write("", "runs", RUNS);
        analyticWriter.write("", "steps", STEPS);
        analyticWriter.write("reference", "normal", "response");

        for (Point point : points) {
            analyticWriter.write(point.reference, point.normal, point.response);
        }

        analyticWriter.write("trace node 0, average");
        for (Point point : points) {
            analyticWriter.write(point.trace);
        }

        analyticWriter.close();
    }

}

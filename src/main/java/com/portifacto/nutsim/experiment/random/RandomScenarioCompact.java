package com.portifacto.nutsim.experiment.random;


import com.portifacto.nutsim.analytics.Analytics;
import com.portifacto.nutsim.analytics.CsvWriter;
import com.portifacto.nutsim.common.Belief;
import com.portifacto.nutsim.experiment.ExperimentCommon;
import com.portifacto.nutsim.experiment.IExperiment;
import com.portifacto.nutsim.parameters.*;
import com.portifacto.nutsim.playbook.Playbook;
import com.portifacto.nutsim.playbook.PlaybookException;
import com.portifacto.nutsim.report.Report;
import com.portifacto.nutsim.simulation.Simulation;

import java.util.LinkedList;
import java.util.List;

/**
 * Scenario Compact - small graph, one run
 */
public class RandomScenarioCompact implements ExperimentCommon, IExperiment {

    private static class Point {
        private final double reference;
        private final double failed;
        private final double normal;
        private final List<Double> traceFirstExpectation;
        private final List<Double> traceLastExpectation;
        private final List<Double> avgUnitResponse;

        public Point(double reference, double failed, double normal,
                     List<Double> traceFirstExpectation, List<Double> traceLastExpectation,
                     List<Double> avgUnitResponse) {
            this.reference = reference;
            this.failed = failed;
            this.normal = normal;
            this.traceFirstExpectation = traceFirstExpectation;
            this.traceLastExpectation = traceLastExpectation;
            this.avgUnitResponse = avgUnitResponse;
        }
    }

    public static void main(String[] argv) throws PlaybookException {
        IExperiment experiment = new RandomScenarioCompact();
        try {
            experiment.process();
        } catch (Exception e) {
            System.out.println("Exception: " + e);
        }
    }

    public void process() throws PlaybookException {

        System.out.println("running " + this.getClass().getSimpleName());

        List<Point> points = new LinkedList<>();

        final int reference = 10;
        final int failed = 10;
        final int normal = 100 - reference - failed;

        // run experiment
        Parameters parameters = new Parameters()
                .setTitle(this.getClass().getSimpleName())
                .setRuns(1)
                .setSteps(10)
                .setDensity(3)
                .addRole(new Role()
                        .setLabel(RoleLabel.NORMAL)
                        .setMean(1.0)
                        .setDeviation(0.0)
                        .setFraction(normal)
                        .setJosang(new Josang()
                                .setValue(Belief.UNCERTAINTY)
                                .setRecalculate(true))
                )
                .addRole(new Role()
                        .setLabel(RoleLabel.REFERENCE)
                        .setMean(1.0)
                        .setDeviation(0.0)
                        .setFraction(reference)
                        .setJosang(new Josang()
                                .setValue(Belief.BELIEF)
                                .setRecalculate(false)))
                .addRole(new Role()
                        .setLabel(RoleLabel.FAULTY)
                        .setMean(0.0)
                        .setDeviation(0.0)
                        .setFraction(failed)
                        .setJosang(new Josang()
                                .setValue(Belief.UNCERTAINTY)
                                .setRecalculate(true))
                )
                .setCalculation(new Calculation()
                        .setType(CalculationType.EXPONENTIAL)
                        .setLambda(LAMBDA))
                .setPhenomenon(new Phenomenon()
                        .setValue(1.0))
                .setGraphType(GraphType.RANDOM)
                .setDecay(0.2)
                .setFusion(new Fusion()
                        .setType(FusionType.CONSENSUS))
                .setSize(10);

        Playbook playbook = Playbook.fromParameters(parameters);

        Simulation ewmaSimulation = new Simulation();
        Report report = ewmaSimulation.run(playbook);

        System.out.println(report);

        Analytics analytics = new Analytics(report);

        List<Double> traceFirstExpectation = analytics.nodeExpectationTraceAverage(0);
        List<Double> traceLastExpectation = analytics.nodeExpectationTraceAverage(SIZE - 1);

        List<Double> avgUnitResponse = analytics.averageUnitResponsePerStepBelief();

        points.add(new Point(
                (double) reference / 100.0,
                (double) failed / 100.0,
                (double) normal / 100.0,
                traceFirstExpectation,
                traceLastExpectation,
                avgUnitResponse));


        String path = PATH + this.getClass().getSimpleName() + ".csv";

        CsvWriter analyticWriter = new CsvWriter(path);

        analyticWriter.write("reference", "failed", "normal");

        for (Point point : points) {
            analyticWriter.write(point.reference, point.failed, point.normal);
            analyticWriter.write("", point.traceFirstExpectation);
            analyticWriter.write("", point.traceLastExpectation);
            analyticWriter.write("unit", point.avgUnitResponse);
        }

        analyticWriter.close();
    }

}

package com.portifacto.nutsim.experiment.random;


import com.portifacto.nutsim.analytics.Analytics;
import com.portifacto.nutsim.analytics.CsvWriter;
import com.portifacto.nutsim.common.Belief;
import com.portifacto.nutsim.experiment.ExperimentCommon;
import com.portifacto.nutsim.experiment.IExperiment;
import com.portifacto.nutsim.parameters.*;
import com.portifacto.nutsim.playbook.Playbook;
import com.portifacto.nutsim.playbook.PlaybookException;
import com.portifacto.nutsim.report.Report;
import com.portifacto.nutsim.simulation.Simulation;

import java.util.LinkedList;
import java.util.List;

/**
 * Scenario 3: sudden change in the phenomenon 0 -> 1
 * <p>
 * Various mixes of reference, good and bad
 * metric: time to register the new value (transition at 0.5)
 */
public class RandomScenario3 implements ExperimentCommon, IExperiment {

    private static class Point {
        private final double reference; // fraction of reference ones
        private final double faulty;    // fraction of faulty ones
        private final double crossing;      // step to cross from lo to hi
        private final List<Double> unitResponse;

        public Point(double reference, double faulty, double crossing, List<Double> unitResponse) {
            this.reference = reference;
            this.faulty = faulty;
            this.crossing = crossing;
            this.unitResponse = unitResponse;
        }
    }

    private static class Row {
        private final double reference;
        private final List<Point> points;

        public Row(double reference, List<Point> points) {
            this.reference = reference;
            this.points = points;
        }
    }


    public static void main(String[] argv) throws PlaybookException {
        IExperiment experiment = new RandomScenario3();
        try {
            experiment.process();
        } catch (Exception e) {
            System.out.println("Exception: " + e.toString());
        }
    }

    public void process() throws PlaybookException {

        System.out.println("running " + this.getClass().getSimpleName());

        List<Row> rows = new LinkedList<>();

        for (int reference = 0; reference <= 25; reference += 25) { // percentage

            List<Point> points = new LinkedList<>();

            for (int faulty = 0; faulty <= 100 - reference; faulty += 10) { // percentage
                int normal = 100 - reference - faulty;

                System.out.println("    reference: " + reference +
                        "%, faulty: " + faulty + "%, normal: "
                        + normal);

                // run experiment
                Parameters parameters = new Parameters()
                        .setTitle(this.getClass().getSimpleName())
                        .setRuns(RUNS)
                        .setSteps(100)
                        .setDensity(DENSITY)
                        .addRole(new Role()
                                .setLabel(RoleLabel.REFERENCE)
                                .setTracking(true)
                                .setFraction(reference)
                                .setTrustworthiness(new Trustworthiness()
                                        .setValue(1.0)
                                        .setRecalculate(false))
                                .setConfidence(new Confidence()
                                        .setValue(1.0)
                                        .setRecalculate(false))
                                .setJosang(new Josang()
                                        .setValue(Belief.BELIEF)
                                        .setRecalculate(false))
                        )
                        .addRole(new Role()
                                .setLabel(RoleLabel.TRUSTWORTHY)
                                .setTracking(true)
                                .setFraction(normal)
                                .setTrustworthiness(new Trustworthiness()
                                        .setValue(0.5)
                                        .setRecalculate(true))
                                .setConfidence(new Confidence()
                                        .setValue(1.0)
                                        .setRecalculate(false))
                                .setJosang(new Josang()
                                        .setValue(Belief.UNCERTAINTY)
                                        .setRecalculate(true))
                        )
                        .addRole(new Role()
                                .setLabel(RoleLabel.FAULTY)
                                .setMean(0.0)
                                .setDeviation(0.0)
                                .setFraction(faulty)
                                .setTrustworthiness(new Trustworthiness()
                                        .setValue(0.5)
                                        .setRecalculate(true))
                                .setConfidence(new Confidence()
                                        .setValue(1.0)
                                        .setRecalculate(false))
                                .setJosang(new Josang()
                                        .setValue(Belief.UNCERTAINTY)
                                        .setRecalculate(true))
                        )
                        .setCalculation(new Calculation()
                                .setType(CalculationType.EXPONENTIAL)
                                .setLambda(LAMBDA))
                        .setPhenomenon(new Phenomenon()
                                .setValue(0.0)
                                .setAlternative(new Alternative()
                                        .setValue(1.0)
                                        .setStep(25)))
                        .setGraphType(GraphType.RANDOM)
                        .setDecay(DECAY)
                        .setFusion(new Fusion()
                                .setType(FusionType.TRANSITIVITY))
                        .setSize(SIZE);

                Playbook playbook = Playbook.fromParameters(parameters);

                Simulation ewmaSimulation = new Simulation();
                Report report = ewmaSimulation.run(playbook);

                Analytics analytics = new Analytics(report);
                List<Double> unitResponse = analytics.averageUnitResponsePerStepStandard();
                double crossing = calculateTransitionLoHi(unitResponse);

                points.add(new Point((double) reference / 100.0, (double) faulty / 100, crossing, unitResponse));

            }

            rows.add(new Row((double) reference / 100.0, points));


        }
        String path = PATH + this.getClass().getSimpleName() + ".csv";

        CsvWriter analyticWriter = new CsvWriter(path);

        analyticWriter.write("parameters");
        analyticWriter.write("", "density", DENSITY);
        analyticWriter.write("", "decay", DECAY);
        analyticWriter.write("", "size", SIZE);
        analyticWriter.write("", "runs", RUNS);
        analyticWriter.write("", "steps", STEPS);

        analyticWriter.write("transition step, for each reference, each row starts from reference and then bad=0");
        for (Row row : rows) {
            List<Double> data = new LinkedList<>();
            data.add(row.reference);
            for (Point point : row.points) {
                data.add(point.crossing);
            }
            analyticWriter.write(data);
        }

        analyticWriter.write("average unit responses for each step, start from reference and bad=0");
        for (Row row : rows) {
            for (Point point : row.points) {
                List<Double> data = new LinkedList<>();
                data.add(row.reference);
                data.add(point.faulty);
                data.addAll(point.unitResponse);
                analyticWriter.write(data);
            }
        }

        analyticWriter.close();
    }

    double calculateTransitionLoHi(List<Double> data) {
        // first result below or equal 0.5 and next above 0.5
        if (data == null) return Double.NaN;
        if (data.size() < 2) return Double.NaN;
        for (int i = 1; i < data.size(); i++) {
            double from = data.get(i - 1);
            double to = data.get(i);
            if (from == 0.5) return i - 1;
            if (to == 0.5) return i;
            if (from < 0.5 && to > 0.5) {
                double x = (i - 1);
                double fromX = Math.abs(from - 0.5);
                double fromY = Math.abs(to - 0.5);
                double offset = fromX / (fromX + fromY);
                return x + offset;
            }
        }
        return 0.0;
    }


}

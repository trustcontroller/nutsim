package com.portifacto.nutsim.experiment.random;


import com.portifacto.nutsim.analytics.Analytics;
import com.portifacto.nutsim.analytics.CsvWriter;
import com.portifacto.nutsim.experiment.ExperimentCommon;
import com.portifacto.nutsim.experiment.IExperiment;
import com.portifacto.nutsim.parameters.*;
import com.portifacto.nutsim.playbook.Playbook;
import com.portifacto.nutsim.playbook.PlaybookException;
import com.portifacto.nutsim.report.Report;
import com.portifacto.nutsim.simulation.Simulation;

import java.util.LinkedList;
import java.util.List;

/**
 * Scenario 0 - baseline scenario
 */
public class RandomScenario0 implements ExperimentCommon, IExperiment {

    private static class Point {
        private final double failed;
        private final double normal;
        private final double response;
        private final List<Double> traceFirst;
        private final List<Double> traceLast;

        public Point(double failed, double normal, double response, List<Double> traceFirst, List<Double> traceLast) {
            this.failed = failed;
            this.normal = normal;
            this.response = response;
            this.traceFirst = traceFirst;
            this.traceLast = traceLast;
        }
    }

    public static void main(String[] argv) throws PlaybookException {
        IExperiment experiment = new RandomScenario0();
        try {
            experiment.process();
        } catch (Exception e) {
            System.out.println("Exception: " + e);
        }
    }

    public void process() throws PlaybookException {

        System.out.println("running " + this.getClass().getSimpleName());

        List<Point> points = new LinkedList<>();

        for (int failed = 0; failed <= 5; failed += 5) { // percentage

            int normal = 100 - failed ;

            System.out.println("failed: " + failed +
                    "%, normal: " + normal );

            // run experiment
            Parameters parameters = new Parameters()
                    .setTitle(this.getClass().getSimpleName())
                    .setRuns(RUNS)
                    .setSteps(STEPS)
                    .setDensity(DENSITY)
                    .addRole(new Role()
                            .setLabel(RoleLabel.NORMAL)
                            .setMean(1.0)
                            .setDeviation(0.0)
                            .setFraction(normal)
                            .setTrustworthiness(new Trustworthiness()
                                    .setValue(0.5)
                                    .setRecalculate(true))
                            .setConfidence(new Confidence()
                                    .setValue(1.0)
                                    .setRecalculate(false))
                    )
                    .addRole(new Role()
                            .setLabel(RoleLabel.FAULTY)
                            .setMean(0.0)
                            .setDeviation(0.0)
                            .setFraction(failed)
                            .setTrustworthiness(new Trustworthiness()
                                    .setValue(0.5)
                                    .setRecalculate(true))
                            .setConfidence(new Confidence()
                                    .setValue(1.0)
                                    .setRecalculate(false))
                    )
                    .setCalculation(new Calculation()
                            .setType(CalculationType.EXPONENTIAL)
                            .setLambda(LAMBDA))
                    .setPhenomenon(new Phenomenon()
                            .setValue(1.0))
                    .setGraphType(GraphType.RANDOM)
                    .setDecay(DECAY)
                    .setFusion(new Fusion()
                            .setType(FusionType.TRANSITIVITY))
                    .setSize(SIZE);

            Playbook playbook = Playbook.fromParameters(parameters);

            Simulation ewmaSimulation = new Simulation();
            Report report = ewmaSimulation.run(playbook);

//            System.out.println(report.toString());

            Analytics analytics = new Analytics(report);
            List<Double> unitResponse = analytics.averageUnitResponsePerStepStandard();
            List<Double> traceNormal = analytics.nodeStandardTraceAverage(0);
            List<Double> traceFaulty = analytics.nodeStandardTraceAverage(SIZE-1);

            double lastResponse = unitResponse.get(unitResponse.size() - 1);

            points.add(new Point(
                    (double) failed / 100.0,
                    (double) normal / 100.0,
                    lastResponse,
                    traceNormal,
                    traceFaulty));
        }

        String path = PATH + this.getClass().getSimpleName() + ".csv";

        CsvWriter analyticWriter = new CsvWriter(path);

        analyticWriter.write("parameters");
        analyticWriter.write("", "density", DENSITY);
        analyticWriter.write("", "decay", DECAY);
        analyticWriter.write("", "size", SIZE);
        analyticWriter.write("", "runs", RUNS);
        analyticWriter.write("", "steps", STEPS);
        analyticWriter.write("failed", "normal", "response");

        for (Point point : points) {
            analyticWriter.write(point.failed, point.normal, point.response);
        }
        analyticWriter.write("Trace FIRST node");
        for(Point point:points) {
            List<Double> line=new LinkedList<>();
            line.add(point.failed);
            line.addAll(point.traceFirst);
            analyticWriter.write(line);
        }
        analyticWriter.write("Trace LAST node");
        for(Point point:points) {
            List<Double> line=new LinkedList<>();
            line.add(point.failed);
            line.addAll(point.traceLast);
            analyticWriter.write(line);
        }

        analyticWriter.close();
    }

}

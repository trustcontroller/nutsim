package com.portifacto.nutsim.experiment.random;


import com.portifacto.nutsim.analytics.Analytics;
import com.portifacto.nutsim.analytics.CsvWriter;
import com.portifacto.nutsim.common.Belief;
import com.portifacto.nutsim.experiment.ExperimentCommon;
import com.portifacto.nutsim.experiment.ExperimentHelper;
import com.portifacto.nutsim.experiment.IExperiment;
import com.portifacto.nutsim.parameters.*;
import com.portifacto.nutsim.playbook.Playbook;
import com.portifacto.nutsim.playbook.PlaybookException;
import com.portifacto.nutsim.report.Report;
import com.portifacto.nutsim.simulation.Simulation;

import java.util.LinkedList;
import java.util.List;

/**
 * Scenario 2 - systematic errors. Mixed proportions of 'good' and 'bad'
 * with some 'good' replaced by reference. Metrics: resilience and robustness.
 */
public class RandomScenario2 extends ExperimentHelper implements ExperimentCommon, IExperiment {

    public static void main(String[] argv) throws PlaybookException {
        IExperiment experiment = new RandomScenario2();
        try {
            experiment.process();
        } catch (Exception e) {
            System.out.println("Exception: " + e.toString());
        }
    }

    public void process() throws PlaybookException {

        System.out.println("running " + this.getClass().getSimpleName());

        List<Line> results = new LinkedList<>();

        for (int reference = 0; reference <= 20; reference += 1) { // percentage

            List<Point> points = new LinkedList<>();

            for (int faulty = 0; faulty <= 100 - reference; faulty += INCREMENT) { // percentage
                int trustworthy = 100 - reference - faulty;

                System.out.println("reference: " + reference +
                        "%, faulty: " + faulty + "%, trustworthy: "
                        + trustworthy);

                // run experiment
                Parameters parameters = new Parameters()
                        .setTitle(this.getClass().getSimpleName())
                        .setRuns(RUNS)
                        .setSteps(STEPS)
                        .setDensity(DENSITY)
                        .addRole(new Role()
                                .setLabel(RoleLabel.REFERENCE)
                                .setMean(1.0)
                                .setDeviation(0.0)
                                .setFraction(reference)
                                .setTrustworthiness(new Trustworthiness()
                                        .setValue(1.0)
                                        .setRecalculate(false))
                                .setConfidence(new Confidence()
                                        .setValue(1.0)
                                        .setRecalculate(false))
                                .setJosang(new Josang()
                                        .setValue(Belief.BELIEF)
                                        .setRecalculate(false))
                        )
                        .addRole(new Role()
                                .setLabel(RoleLabel.TRUSTWORTHY)
                                .setMean(1.0)
                                .setDeviation(0.0)
                                .setFraction(trustworthy)
                                .setTrustworthiness(new Trustworthiness()
                                        .setValue(0.5)
                                        .setRecalculate(true))
                                .setConfidence(new Confidence()
                                        .setValue(1.0)
                                        .setRecalculate(false))
                                .setJosang(new Josang()
                                        .setValue(Belief.UNCERTAINTY)
                                        .setRecalculate(true))
                        )
                        .addRole(new Role()
                                .setLabel(RoleLabel.FAULTY)
                                .setMean(0.0)
                                .setDeviation(0.0)
                                .setFraction(faulty)
                                .setTrustworthiness(new Trustworthiness()
                                        .setValue(0.5)
                                        .setRecalculate(true))
                                .setConfidence(new Confidence()
                                        .setValue(1.0)
                                        .setRecalculate(false))
                                .setJosang(new Josang()
                                        .setValue(Belief.UNCERTAINTY)
                                        .setRecalculate(true))
                        )
                        .setCalculation(new Calculation()
                                .setType(CalculationType.EXPONENTIAL)
                                .setLambda(LAMBDA))
                        .setPhenomenon(new Phenomenon()
                                .setValue(1.0))
                        .setGraphType(GraphType.RANDOM)
                        .setDecay(DECAY)
                        .setFusion(new Fusion()
                                .setType(FusionType.TRANSITIVITY))
                        .setSize(SIZE);

                Playbook playbook = Playbook.fromParameters(parameters);

                Simulation ewmaSimulation = new Simulation();
                Report report = ewmaSimulation.run(playbook);

                Analytics analytics = new Analytics(report);
                List<Double> unitResponse = analytics.averageUnitResponsePerStepStandard();
                double lastResponse = unitResponse.get(unitResponse.size() - 1);

                points.add(new Point(lastResponse,
                        (double) reference / 100.0, (double) faulty / 100.0, (double) trustworthy / 100.0));

            }

            double resilience = calculateTransitionLoHi(points);
            double robustness = calculateRobustness(points, (double) INCREMENT / 100.0);
            results.add(new Line((double) reference / 100.0, resilience, robustness, points));

        }
        String path = PATH + this.getClass().getSimpleName() + ".csv";

        CsvWriter analyticWriter = new CsvWriter(path);

        analyticWriter.write("parameters");
        analyticWriter.write("", "density", DENSITY);
        analyticWriter.write("", "decay", DECAY);
        analyticWriter.write("", "size", SIZE);
        analyticWriter.write("", "runs", RUNS);
        analyticWriter.write("", "steps", STEPS);
        analyticWriter.write("reference", "resilience", "robustness");
        for (Line result : results) {
            analyticWriter.write(result.reference, result.resilience, result.robustness);
        }

        analyticWriter.write("last unit responses, starts from reference=0, bad=0");
        for (Line line : results) {
            List<Double> lur = new LinkedList<>();
            for (Point point : line.points) {
                lur.add(point.lastUnitResponse);
            }
            analyticWriter.write(lur);
        }

        analyticWriter.close();
    }


}

package com.portifacto.nutsim.experiment.random;


import com.portifacto.nutsim.analytics.Analytics;
import com.portifacto.nutsim.analytics.CsvWriter;
import com.portifacto.nutsim.common.Belief;
import com.portifacto.nutsim.experiment.ExperimentCommon;
import com.portifacto.nutsim.experiment.IExperiment;
import com.portifacto.nutsim.parameters.*;
import com.portifacto.nutsim.playbook.Playbook;
import com.portifacto.nutsim.playbook.PlaybookException;
import com.portifacto.nutsim.report.Report;
import com.portifacto.nutsim.simulation.Simulation;

import java.util.LinkedList;
import java.util.List;

/**
 * Scenario 4: long-term drift
 */
public class RandomScenario4 implements ExperimentCommon, IExperiment {

    private static final double DRIFT = -0.02;

    private class Point {
        private double reference;
        private double mediocre;
        private double response;
        private int transition;
        List<Double> unitResponse;
        List<Double> avgTrustworthiness;
        List<Double> avgExpectation;
        List<Double> avgExpectationResponse;
        List<Double> avgPlausibilityResponse;

        public Point(double reference, double mediocre, double response, int transition, List<Double> unitResponse,
                     List<Double> avgTrustworthiness,
                     List<Double> avgExpectation, List<Double> avgExpectationResponse,
                     List<Double> avgPlausibilityResponse) {
            this.reference = reference;
            this.mediocre = mediocre;
            this.response = response;
            this.transition = transition;
            this.unitResponse = unitResponse;
            this.avgTrustworthiness = avgTrustworthiness;
            this.avgExpectation=avgExpectation;
            this.avgExpectationResponse=avgExpectationResponse;
            this.avgPlausibilityResponse=avgPlausibilityResponse;
        }
    }

    public static void main(String[] argv) throws PlaybookException {
        IExperiment experiment = new RandomScenario4();
        try {
            experiment.process();
        } catch (Exception e) {
            System.out.println("Exception: " + e.toString());
        }
    }

    public void process() throws PlaybookException {

        System.out.println("running " + this.getClass().getSimpleName());

        List<Point> points = new LinkedList<>();

        for (int reference = 0; reference <= 20; reference += 1) { // percentage

            int normal = 100 - reference;

            System.out.println("reference: " + reference +
                    "%, normal: " + normal + "%");

            // run experiment
            Parameters parameters = new Parameters()
                    .setTitle(this.getClass().getSimpleName())
                    .setRuns(RUNS)
                    .setSteps(100)
                    .setDensity(DENSITY)
                    .addRole(new Role()
                            .setLabel(RoleLabel.REFERENCE)
                            .setMean(1.0)
                            .setDeviation(0.0)
                            .setFraction(reference)
                            .setJosang(new Josang()
                                    .setValue(Belief.BELIEF)
                                    .setRecalculate(false) )
                    )
                    .addRole(new Role()
                            .setLabel(RoleLabel.NORMAL)
                            .setMean(1.0)
                            .setDeviation(0.0)
                            .setDrift(DRIFT)
                            .setDistortionFrom(20)
                            .setDistortionTo(Integer.MAX_VALUE)
                            .setFraction(normal)
                            .setJosang(new Josang()
                                    .setValue(Belief.UNCERTAINTY)
                                    .setRecalculate(true) )
                    )
                    .setCalculation(new Calculation()
                            .setType(CalculationType.EXPONENTIAL)
                            .setLambda(LAMBDA))
                    .setPhenomenon(new Phenomenon()
                            .setValue(1.0))
                    .setGraphType(GraphType.RANDOM)
                    .setFusion(new Fusion()
                            .setType(FusionType.CONSENSUS))
                    .setDecay(DECAY)
                    .setSize(SIZE);

            Playbook playbook = Playbook.fromParameters(parameters);

            Simulation ewmaSimulation = new Simulation();
            Report report = ewmaSimulation.run(playbook);

//            System.out.println(report.toString());

            Analytics analytics = new Analytics(report);


            List<Double> unitResponse = analytics.averageUnitResponsePerStepStandard();
            List<Double> avgTrustworthiness = analytics.averageTrustworthinessPerStepStandard();
            List<Double> avgExpectation=analytics.averageExpectationPerStepBelief();
            List<Double> avgExpectationResponse=analytics.averageUnitResponsePerStepBelief();
            List<Double> avgPlausibilityResponse=analytics.averageUnitResponsePerStepPlausibility();
            double lastResponse = unitResponse.get(unitResponse.size() - 1);

            int transition = 0;
            for (int i = 1; i < unitResponse.size(); i++) {
                if ((unitResponse.get(i - 1) >= 0.5) && (unitResponse.get(i) < 0.5)) transition = i;
            }

            points.add(new Point(
                    (double) reference / 100.0, (double) normal / 100.0, (double) lastResponse,
                    transition, unitResponse, avgTrustworthiness,avgExpectation,avgExpectationResponse,
                    avgPlausibilityResponse));

        }

        String path = PATH + this.getClass().getSimpleName() + ".csv";

        CsvWriter analyticWriter = new CsvWriter(path);

        analyticWriter.write("parameters");
        analyticWriter.write("", "density", DENSITY);
        analyticWriter.write("", "decay", DECAY);
        analyticWriter.write("", "size", SIZE);
        analyticWriter.write("", "runs", RUNS);
        analyticWriter.write("", "steps", STEPS);
        analyticWriter.write("", "drift", DRIFT);
        analyticWriter.write("reference", "mediocre", "response", "transition");

        for (Point point : points) {
            analyticWriter.write(point.reference, point.mediocre, point.response, point.transition);
        }

        analyticWriter.write();
        analyticWriter.write("average unit response per step for various fractions of reference");
        for (Point point : points) {
            analyticWriter.write(point.unitResponse);
        }

        analyticWriter.write();
        analyticWriter.write("average trustworthiness per step for various fractions of reference");
        for (Point point : points) {
            analyticWriter.write(point.avgTrustworthiness);
        }

        analyticWriter.write();
        analyticWriter.write("average expectation per step for various fractions of reference");
        for (Point point : points) {
            analyticWriter.write(point.avgExpectation);
        }

        analyticWriter.write();
        analyticWriter.write("average unit response out of expectation per step for various fractions of reference");
        for (Point point : points) {
            analyticWriter.write(point.avgExpectationResponse);
        }

        analyticWriter.write();
        analyticWriter.write("average unit response out of plausibility per step for various fractions of reference");
        for (Point point : points) {
            analyticWriter.write(point.avgPlausibilityResponse);
        }

        analyticWriter.close();
    }


}

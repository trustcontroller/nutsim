package com.portifacto.nutsim.experiment.random;


import com.portifacto.nutsim.analytics.Analytics;
import com.portifacto.nutsim.analytics.CsvWriter;
import com.portifacto.nutsim.common.Belief;
import com.portifacto.nutsim.experiment.ExperimentCommon;
import com.portifacto.nutsim.experiment.IExperiment;
import com.portifacto.nutsim.parameters.*;
import com.portifacto.nutsim.playbook.Playbook;
import com.portifacto.nutsim.playbook.PlaybookException;
import com.portifacto.nutsim.report.Report;
import com.portifacto.nutsim.simulation.Simulation;

import java.util.LinkedList;
import java.util.List;

/**
 * Scenario B - analysis of the boostrap process
 */
public class RandomScenarioB implements ExperimentCommon, IExperiment {

    private static class Point {
        private final double reference;
        private final double failed;
        private final double normal;
        private final double density;
        private final double decay;
        private final double initial;
        private final double normalEstablished; // above 0.9 within steps
        private final double failedEstablished; // below 0.1 within steps
        private final List<Double> traceFirst;
        private final List<Double> traceLast;
        private final List<Double> traceFirstPlausibility;
        private final List<Double> traceLastPlausibility;
        private final List<Double> traceFirstExpectation;
        private final List<Double> traceLastExpectation;
        private final List<Double> avgExpectationResponse;

        public Point(double reference, double failed, double normal,
                     double density, double decay, double initial,
                     double normalEstablished, double failedEstablished,
                     List<Double> traceFirst, List<Double> traceLast,
                     List<Double> traceFirstPlausibility,List<Double> traceLastPlausibility,
                     List<Double> traceFirstExpectation, List<Double> traceLastExpectation,
                     List<Double> avgExpectationResponse) {
            this.reference=reference;
            this.failed = failed;
            this.normal = normal;
            this.density = density;
            this.decay=decay;
            this.initial=initial;
            this.normalEstablished=normalEstablished;
            this.failedEstablished=failedEstablished;
            this.traceFirst = traceFirst;
            this.traceLast = traceLast;
            this.traceFirstPlausibility = traceFirstPlausibility;
            this.traceLastPlausibility = traceLastPlausibility;
            this.traceFirstExpectation = traceFirstExpectation;
            this.traceLastExpectation = traceLastExpectation;
            this.avgExpectationResponse=avgExpectationResponse;
        }
    }

    public static void main(String[] argv) throws PlaybookException {
        IExperiment experiment = new RandomScenarioB();
        try {
            experiment.process();
        } catch (Exception e) {
            System.out.println("Exception: " + e);
        }
    }

    public void process() throws PlaybookException {

        System.out.println("running " + this.getClass().getSimpleName());

        List<Point> points = new LinkedList<>();

        int count=0;
        for(int density=3;density<=3;density++) {

            double[] decays={DECAY};
            for(double decay:decays) {

                double[] initials={0.5};
                for(double initial:initials) {

                    for (int reference = 0; reference <= 10; reference+=1) {

                    int failed=10;
                    int normal=100-failed-reference;

                        System.out.println( "  "+(count++) +
                                ": reference "+ reference+
                                "% failed: " + failed +
                                "%, normal: " + normal +
                                "% density "+density +
                                " decay "+decay +
                                " initial "+initial);

                        // run experiment
                        Parameters parameters = new Parameters()
                                .setTitle(this.getClass().getSimpleName())
                                .setRuns(RUNS)
                                .setSteps(STEPS)
                                .setDensity(density)
                                .addRole(new Role()
                                        .setLabel(RoleLabel.NORMAL)
                                        .setMean(1.0)
                                        .setDeviation(0.0)
                                        .setFraction(normal)
                                        .setTrustworthiness(new Trustworthiness()
                                                .setValue(initial)
                                                .setRecalculate(true))
                                        .setConfidence(new Confidence()
                                                .setValue(0.0)
                                                .setRecalculate(true)
                                                .setType(ConfidenceType.FROM_BELIEF))
                                        .setJosang(new Josang()
                                                .setValue(Belief.UNCERTAINTY)
                                                .setRecalculate(true))
                                )
                                .addRole(new Role()
                                        .setLabel(RoleLabel.REFERENCE)
                                        .setMean(1.0)
                                        .setDeviation(0.0)
                                        .setFraction(reference)
                                        .setTrustworthiness(new Trustworthiness()
                                                .setValue(1.0)
                                                .setRecalculate(false))
                                        .setConfidence(new Confidence()
                                                .setValue(1.0)
                                                .setRecalculate(false)
                                                .setType(ConfidenceType.DEFINED))
                                        .setJosang(new Josang()
                                                .setValue(Belief.BELIEF)
                                                .setRecalculate(false)))
                                .addRole(new Role()
                                        .setLabel(RoleLabel.FAULTY)
                                        .setMean(0.0)
                                        .setDeviation(0.0)
                                        .setFraction(failed)
                                        .setTrustworthiness(new Trustworthiness()
                                                .setValue(initial)
                                                .setRecalculate(true))
                                        .setConfidence(new Confidence()
                                                .setValue(0.0)
                                                .setRecalculate(true)
                                                .setType(ConfidenceType.FROM_BELIEF))
                                        .setJosang(new Josang()
                                                .setValue(Belief.UNCERTAINTY)
                                                .setRecalculate(true))
                                )
                                .setCalculation(new Calculation()
                                        .setType(CalculationType.EXPONENTIAL)
                                        .setLambda(LAMBDA))
                                .setPhenomenon(new Phenomenon()
                                        .setValue(1.0))
                                .setGraphType(GraphType.RANDOM)
                                .setDecay(decay)
                                .setFusion(new Fusion()
                                        .setType(FusionType.CONSENSUS))
                                .setSize(SIZE);

                        Playbook playbook = Playbook.fromParameters(parameters);

                        Simulation ewmaSimulation = new Simulation();
                        Report report = ewmaSimulation.run(playbook);

                        Analytics analytics = new Analytics(report);
                        List<Double> traceFirst = analytics.nodeStandardTraceAverage(0);
                        List<Double> traceLast = analytics.nodeStandardTraceAverage(SIZE-1);

                        List<Double> traceFirstExpectation=analytics.nodeExpectationTraceAverage(0);
                        List<Double> traceLastExpectation=analytics.nodeExpectationTraceAverage(SIZE-1);

                        List<Double> traceFirstPlausibility=analytics.nodePlausibilityTraceAverage(0);
                        List<Double> traceLastPlausibility=analytics.nodePlausibilityTraceAverage(SIZE-1);

                        List<Double> avgExpectationResponse=analytics.averageUnitResponsePerStepBelief();

                        double normalEstablished=checkTrace(traceFirst,1,0.8);
                        double failedEstablished=checkTrace(traceLast,-1,0.2);

                        points.add(new Point(
                                (double) reference / 100.0,
                                (double) failed / 100.0,
                                (double)normal/100.0,
                                density,
                                decay,
                                initial,
                                normalEstablished,
                                failedEstablished,
                                traceFirst,
                                traceLast,
                                traceFirstPlausibility,
                                traceLastPlausibility,
                                traceFirstExpectation,
                                traceLastExpectation,
                                avgExpectationResponse));

                    }
                }
            }
        }

        String path = PATH + this.getClass().getSimpleName() + ".csv";

        CsvWriter analyticWriter = new CsvWriter(path);

        analyticWriter.write("reference","failed", "normal",
                "density","decay","initial","estNormal","estFailed","unitResp");

        for (Point point : points) {
            analyticWriter.write(point.reference,point.failed, point.normal,
                    point.density,point.decay,point.initial,
                    point.normalEstablished,point.failedEstablished,
                    point.avgExpectationResponse.get(point.avgExpectationResponse.size()-1));
            analyticWriter.write("",point.traceFirst);
            analyticWriter.write("",point.traceLast);
            analyticWriter.write("",point.traceFirstExpectation);
            analyticWriter.write("",point.traceLastExpectation);
            analyticWriter.write("",point.traceFirstPlausibility);
            analyticWriter.write("",point.traceLastPlausibility);
            analyticWriter.write("unit",point.avgExpectationResponse);
        }

        analyticWriter.close();
    }

    private int checkTrace(List<Double> trace,int comparator,double value) {
        for(int i=0;i<trace.size();i++) {
            if(trace.get(i).compareTo(value)==comparator) {
                return i+1;
            }
        }
        return 0;
    }

}

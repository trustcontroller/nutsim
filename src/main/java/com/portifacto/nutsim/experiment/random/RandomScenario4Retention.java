package com.portifacto.nutsim.experiment.random;


import com.portifacto.nutsim.analytics.Analytics;
import com.portifacto.nutsim.analytics.CsvWriter;
import com.portifacto.nutsim.experiment.ExperimentCommon;
import com.portifacto.nutsim.experiment.IExperiment;
import com.portifacto.nutsim.parameters.*;
import com.portifacto.nutsim.playbook.Playbook;
import com.portifacto.nutsim.playbook.PlaybookException;
import com.portifacto.nutsim.report.Report;
import com.portifacto.nutsim.simulation.Simulation;

import java.util.LinkedList;
import java.util.List;

/**
 * Scenario 4: long-term drift
 */
public class RandomScenario4Retention implements ExperimentCommon, IExperiment {

    private static final double DRIFT = -0.02;

    private static final int REFERENCE = 20;

    private class Point {
        private double decay;
        private double response;
        private int transition;
        List<Double> unitResponse;
        List<Double> avgTrustworthiness;

        public Point(double decay, double response, int transition, List<Double> unitResponse, List<Double> avgTrustworthiness) {
            this.decay = decay;
            this.response = response;
            this.transition = transition;
            this.unitResponse = unitResponse;
            this.avgTrustworthiness = avgTrustworthiness;
        }
    }

    public static void main(String[] argv) throws PlaybookException {
        IExperiment experiment = new RandomScenario4Retention();
        try {
            experiment.process();
        } catch (Exception e) {
            System.out.println("Exception: " + e.toString());
        }
    }

    public void process() throws PlaybookException {

        System.out.println("running " + this.getClass().getSimpleName());

        List<Point> points = new LinkedList<>();

        for (int decay = 0; decay <= 100; decay += 5) { // percentage

            System.out.println("decay: " + decay + "%");

            // run experiment
            Parameters parameters = new Parameters()
                    .setTitle(this.getClass().getSimpleName())
                    .setRuns(RUNS)
                    .setSteps(STEPS)
                    .setDensity(4)
                    .addRole(new Role()
                            .setLabel(RoleLabel.REFERENCE)
                            .setMean(1.0)
                            .setDeviation(0.0)
                            .setFraction(REFERENCE)
                            .setTrustworthiness(new Trustworthiness()
                                    .setValue(1.0)
                                    .setRecalculate(false))
                            .setConfidence(new Confidence()
                                    .setValue(1.0)
                                    .setRecalculate(false))
                    )
                    .addRole(new Role()
                            .setLabel(RoleLabel.MEDIOCRE)
                            .setMean(1.0)
                            .setDeviation(0.0)
                            .setDrift(DRIFT)
                            .setFraction(100 - REFERENCE)
                            .setTrustworthiness(new Trustworthiness()
                                    .setValue(0.5)
                                    .setRecalculate(true))
                            .setConfidence(new Confidence()
                                    .setValue(1.0)
                                    .setRecalculate(false))
                    )
                    .setCalculation(new Calculation()
                            .setType(CalculationType.EXPONENTIAL)
                            .setLambda(LAMBDA))
                    .setPhenomenon(new Phenomenon()
                            .setValue(1.0))
                    .setGraphType(GraphType.RANDOM)
                    .setDecay((double) decay / 100)
                    .setSize(SIZE);

            Playbook playbook = Playbook.fromParameters(parameters);

            Simulation ewmaSimulation = new Simulation();
            Report report = ewmaSimulation.run(playbook);

            Analytics analytics = new Analytics(report);
            List<Double> unitResponse = analytics.averageUnitResponsePerStepStandard();
            List<Double> avgTrustworthiness = analytics.averageTrustworthinessPerStepStandard();
            double lastResponse = unitResponse.get(unitResponse.size() - 1);

            int transition = 0;
            for (int i = 1; i < unitResponse.size(); i++) {
                if ((unitResponse.get(i - 1) >= 0.5) && (unitResponse.get(i) < 0.5)) transition = i;
            }

            points.add(new Point(
                    (double) decay / 100.0, (double) lastResponse,
                    transition, unitResponse, avgTrustworthiness));

        }

        String path = PATH + this.getClass().getSimpleName() + ".csv";

        CsvWriter analyticWriter = new CsvWriter(path);

        analyticWriter.write("parameters");
        analyticWriter.write("", "density", DENSITY);
        analyticWriter.write("", "size", SIZE);
        analyticWriter.write("", "runs", RUNS);
        analyticWriter.write("", "steps", STEPS);
        analyticWriter.write("", "drift", DRIFT);
        analyticWriter.write("", "reference", (double) REFERENCE / 100);
        analyticWriter.write("decay", "response", "transition");

        for (Point point : points) {
            analyticWriter.write(point.decay, point.response, point.transition);
        }

        analyticWriter.close();
    }


}

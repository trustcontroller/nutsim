package com.portifacto.nutsim.experiment.random;


import com.portifacto.nutsim.analytics.Analytics;
import com.portifacto.nutsim.analytics.CsvWriter;
import com.portifacto.nutsim.experiment.ExperimentCommon;
import com.portifacto.nutsim.experiment.IExperiment;
import com.portifacto.nutsim.parameters.*;
import com.portifacto.nutsim.playbook.Playbook;
import com.portifacto.nutsim.playbook.PlaybookException;
import com.portifacto.nutsim.report.Report;
import com.portifacto.nutsim.simulation.Simulation;

import java.util.LinkedList;
import java.util.List;

/**
 * Scenario 1: random errors. MIx of mediocre that implement Gaussian noise
 * and some reference
 */
public class RandomScenario1NoEwma implements ExperimentCommon, IExperiment {

    private static final double DEVIATION = 0.1;

    private static class Point {
        private final double reference;
        private final double mediocre;
        private final double response;

        public Point(double reference, double mediocre, double response) {
            this.reference = reference;
            this.mediocre = mediocre;
            this.response = response;
        }
    }

    public static void main(String[] argv) throws PlaybookException {
        IExperiment experiment = new RandomScenario1NoEwma();
        try {
            experiment.process();
        } catch (Exception e) {
            System.out.println("Exception: " + e.toString());
        }
    }


    public void process() throws PlaybookException {

        System.out.println("running " + this.getClass().getSimpleName());


        List<Point> points = new LinkedList<>();

        for (int reference = 0; reference <= 100; reference += 1) { // percentage

            int mediocre = 100 - reference;

            System.out.println("reference: " + reference +
                    "%, mediocre: " + mediocre + "%");

            // run experiment
            Parameters parameters = new Parameters()
                    .setTitle(this.getClass().getSimpleName())
                    .setRuns(RUNS)
                    .setSteps(STEPS)
                    .setDensity(0)
                    .addRole(new Role()
                            .setLabel(RoleLabel.REFERENCE)
                            .setMean(1.0)
                            .setDeviation(0.0)
                            .setFraction(reference)
                            .setTrustworthiness(new Trustworthiness()
                                    .setValue(1.0)
                                    .setRecalculate(false))
                            .setConfidence(new Confidence()
                                    .setValue(1.0)
                                    .setRecalculate(false))
                    )
                    .addRole(new Role()
                            .setLabel(RoleLabel.MEDIOCRE)
                            .setMean(1.0)
                            .setDeviation(DEVIATION)
                            .setFraction(mediocre)
                            .setTrustworthiness(new Trustworthiness()
                                    .setValue(0.5)
                                    .setRecalculate(true))
                            .setConfidence(new Confidence()
                                    .setValue(1.0)
                                    .setRecalculate(false))
                    )
                    .setCalculation(new Calculation()
                            .setType(CalculationType.EXPONENTIAL)
                            .setLambda(LAMBDA))
                    .setPhenomenon(new Phenomenon()
                            .setValue(1.0))
                    .setGraphType(GraphType.RANDOM)
                    .setDecay(0)
                    .setSize(SIZE);

            Playbook playbook = Playbook.fromParameters(parameters);

            Simulation ewmaSimulation = new Simulation();
            Report report = ewmaSimulation.run(playbook);

            Analytics analytics = new Analytics(report);
            List<Double> unitResponse = analytics.averageUnitResponsePerStepStandard();
            double lastResponse1 = unitResponse.get(unitResponse.size() - 1);
            double lastResponse2 = unitResponse.get(unitResponse.size() - 2);
            double lastResponse3 = unitResponse.get(unitResponse.size() - 3);
            double lastResponse = (lastResponse1 + lastResponse2 + lastResponse3) / 3;

            points.add(new Point(
                    (double) reference / 100.0, (double) mediocre / 100.0, (double) lastResponse));

        }

        String path = PATH + this.getClass().getSimpleName() + ".csv";

        CsvWriter analyticWriter = new CsvWriter(path);

        analyticWriter.write("parameters");
        analyticWriter.write("", "density", DENSITY);
        analyticWriter.write("", "decay", DECAY);
        analyticWriter.write("", "size", SIZE);
        analyticWriter.write("", "runs", RUNS);
        analyticWriter.write("", "steps", STEPS);
        analyticWriter.write("", "deviation", DEVIATION);
        analyticWriter.write("reference", "mediocre", "response");

        for (Point point : points) {
            analyticWriter.write(point.reference, point.mediocre, point.response);
        }

        analyticWriter.close();
    }


}

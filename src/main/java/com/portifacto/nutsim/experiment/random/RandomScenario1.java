package com.portifacto.nutsim.experiment.random;

import com.portifacto.nutsim.analytics.Analytics;
import com.portifacto.nutsim.analytics.CsvWriter;
import com.portifacto.nutsim.experiment.ExperimentCommon;
import com.portifacto.nutsim.experiment.IExperiment;
import com.portifacto.nutsim.parameters.*;
import com.portifacto.nutsim.playbook.Playbook;
import com.portifacto.nutsim.playbook.PlaybookException;
import com.portifacto.nutsim.report.Report;
import com.portifacto.nutsim.simulation.Simulation;

import java.util.LinkedList;
import java.util.List;

/**
 * Scenario 1: random errors. Mix of normal that implement Gaussian noise
 * and some reference
 */
public class RandomScenario1 implements ExperimentCommon, IExperiment {

    private static final double DEVIATION = 0.2;

    private static class Point {
        private final double reference;
        private final double normal;
        private final double response;

        public Point(double reference, double normal, double response) {
            this.reference = reference;
            this.normal = normal;
            this.response = response;
        }
    }

    public static void main(String[] argv) throws PlaybookException {
        IExperiment experiment = new RandomScenario1();
        try {
            experiment.process();
        } catch (Exception e) {
            System.out.println("Exception: " + e.toString());
        }
    }

    public void process() throws PlaybookException {

        System.out.println("running " + this.getClass().getSimpleName());


        List<Point> points = new LinkedList<>();

        for (int reference = 0; reference <= 100; reference += ExperimentCommon.INCREMENT) { // percentage

            int normal = 100 - reference;

            System.out.println("reference: " + reference +
                    "%, normal: " + normal + "%");

            // run experiment
            Parameters parameters = new Parameters()
                    .setTitle(this.getClass().getSimpleName())
                    .setRuns(RUNS)
                    .setSteps(STEPS)
                    .setDensity(DENSITY)
                    .addRole(new Role()
                            .setLabel(RoleLabel.REFERENCE)
                            .setMean(1.0)
                            .setDeviation(0.0)
                            .setFraction(reference)
                            .setTrustworthiness(new Trustworthiness()
                                    .setValue(1.0)
                                    .setRecalculate(false))
                            .setConfidence(new Confidence()
                                    .setValue(1.0)
                                    .setRecalculate(false))
                    )
                    .addRole(new Role()
                            .setLabel(RoleLabel.NORMAL)
                            .setMean(1.0)
                            .setDeviation(DEVIATION)
                            .setDistortionFrom(0)
                            .setDistortionTo(Integer.MAX_VALUE)
                            .setFraction(normal)
                            .setTrustworthiness(new Trustworthiness()
                                    .setValue(0.5)
                                    .setRecalculate(true))
                            .setConfidence(new Confidence()
                                    .setValue(1.0)
                                    .setRecalculate(false))
                    )
                    .setCalculation(new Calculation()
                            .setType(CalculationType.EXPONENTIAL)
                            .setLambda(LAMBDA))
                    .setPhenomenon(new Phenomenon()
                            .setValue(1.0))
                    .setGraphType(GraphType.RANDOM)
                    .setDecay(DECAY)
                    .setFusion(new Fusion()
                            .setType(FusionType.TRANSITIVITY))
                    .setSize(SIZE);

            Playbook playbook = Playbook.fromParameters(parameters);

            Simulation ewmaSimulation = new Simulation();
            Report report = ewmaSimulation.run(playbook);

            Analytics analytics = new Analytics(report);
            List<Double> unitResponse = analytics.averageUnitResponsePerStepStandard();
            double lastResponse1 = unitResponse.get(unitResponse.size() - 1);
            double lastResponse2 = unitResponse.get(unitResponse.size() - 2);
            double lastResponse3 = unitResponse.get(unitResponse.size() - 3);
            double lastResponse = (lastResponse1 + lastResponse2 + lastResponse3) / 3;

            points.add(new Point(
                    (double) reference / 100.0, (double) normal / 100.0, (double) lastResponse));

        }

        String path = PATH + this.getClass().getSimpleName() + ".csv";

        CsvWriter analyticWriter = new CsvWriter(path);

        analyticWriter.write("parameters");
        analyticWriter.write("", "density", ExperimentCommon.DENSITY);
        analyticWriter.write("", "decay", ExperimentCommon.DECAY);
        analyticWriter.write("", "size", ExperimentCommon.SIZE);
        analyticWriter.write("", "runs", ExperimentCommon.RUNS);
        analyticWriter.write("", "steps", ExperimentCommon.STEPS);
        analyticWriter.write("", "deviation", DEVIATION);
        analyticWriter.write("reference", "normal", "response");

        for (Point point : points) {
            analyticWriter.write(point.reference, point.normal, point.response);
        }

        analyticWriter.close();
    }


}

package com.portifacto.nutsim.experiment;

public interface ExperimentCommon {

    int SIZE = 100;
    int STEPS = 50;
    int RUNS = 50;
    int DENSITY = 3;
    double DECAY = 0.2;

    int INCREMENT = 1;

    double LAMBDA = 2.4;

    String PATH = "C:\\home\\piotr\\repository\\nutsim\\results\\";


}

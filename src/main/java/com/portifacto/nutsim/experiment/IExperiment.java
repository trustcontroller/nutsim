package com.portifacto.nutsim.experiment;

import com.portifacto.nutsim.playbook.PlaybookException;

public interface IExperiment {

    public void process() throws PlaybookException;
}

package com.portifacto.nutsim.parameters;

public enum ConfidenceType {
    DEFINED,
    FROM_BELIEF;
}

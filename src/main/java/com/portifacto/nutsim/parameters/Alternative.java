package com.portifacto.nutsim.parameters;

import org.nd4j.shade.jackson.annotation.JsonAutoDetect;
import org.nd4j.shade.jackson.annotation.JsonIgnoreProperties;
import org.nd4j.shade.jackson.annotation.JsonInclude;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonIgnoreProperties(ignoreUnknown = true)
@JsonAutoDetect(getterVisibility = JsonAutoDetect.Visibility.NONE, setterVisibility = JsonAutoDetect.Visibility.NONE)
public class Alternative {

    private final static double DEFAULT_ALT_MEAN = 0;
    private final static double DEFAULT_ALT_DEVIATION = 0;
    private final static double DEFAULT_ALT_DRIFT = 0;

    private final static int DEFAULT_STEP = Integer.MAX_VALUE;

    public Integer step = null;

    public Double value = null;

    public Double deviation = null;

    public Double drift = null;

    public Confidence confidence = null;

    public Trustworthiness trustworthiness = null;

    public Alternative() {

    }

    public Alternative setStep(int step) {
        this.step = step;
        return this;
    }

    public int getStep() {
        return step == null ? DEFAULT_STEP : step;
    }

    public Alternative setValue(double value) {
        this.value = value;
        return this;
    }

    public double getValue() {
        return value == null ? DEFAULT_ALT_MEAN : value;
    }

    public Alternative setDeviation(double deviation) {
        this.deviation = deviation;
        return this;
    }

    public double getDeviation() {
        return deviation == null ? DEFAULT_ALT_DEVIATION : deviation;
    }

    public Alternative setDrift(double drift) {
        this.drift = drift;
        return this;
    }

    public double getDrift() {
        return drift == null ? DEFAULT_ALT_DRIFT : drift;
    }

    public Alternative setConfidence(Confidence confidence) {
        this.confidence = confidence;
        return this;
    }

    public Confidence getConfidence() {
        if (confidence == null) return new Confidence();
        return confidence;
    }

    public Alternative setTrustworthiness(Trustworthiness trustworthiness) {
        this.trustworthiness = trustworthiness;
        return this;
    }

    public Trustworthiness getTrustworthiness() {
        if (trustworthiness == null) return new Trustworthiness();
        return trustworthiness;
    }


}

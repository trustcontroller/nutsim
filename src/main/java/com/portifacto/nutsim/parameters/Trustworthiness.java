package com.portifacto.nutsim.parameters;

import org.nd4j.shade.jackson.annotation.JsonAutoDetect;
import org.nd4j.shade.jackson.annotation.JsonIgnoreProperties;
import org.nd4j.shade.jackson.annotation.JsonInclude;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonIgnoreProperties(ignoreUnknown = true)
@JsonAutoDetect(getterVisibility = JsonAutoDetect.Visibility.NONE, setterVisibility = JsonAutoDetect.Visibility.NONE)
public class Trustworthiness {

    private static final double DEFAULT_VALUE = 0.5;
    private static final boolean DEFAULT_RECALCULATE = true;

    public Double value = null;
    public Boolean recalculate = null;

    public Trustworthiness() {

    }

    public double getValue() {
        return value == null ? DEFAULT_VALUE : value;
    }

    public boolean getRecalculate() {
        return recalculate == null ? DEFAULT_RECALCULATE : recalculate;
    }

    public Trustworthiness setValue(double value) {
        if (value < 0 || value > 1) throw new IllegalArgumentException();
        this.value = value;
        return this;
    }

    public Trustworthiness setRecalculate(boolean recalculate) {
        this.recalculate = recalculate;
        return this;
    }


}

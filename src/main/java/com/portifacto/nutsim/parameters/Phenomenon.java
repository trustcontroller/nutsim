package com.portifacto.nutsim.parameters;

import org.nd4j.shade.jackson.annotation.JsonIgnoreProperties;
import org.nd4j.shade.jackson.annotation.JsonInclude;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonIgnoreProperties(ignoreUnknown = true)

public class Phenomenon {

    public Double value =null;

    public Double atZeroX=null;
    public Double  atZeroY=null;
    public Double  incrementX=null;
    public Double  incrementY=null;

    public Double rangeFrom=null;
    public Double rangeTo=null;
    public Double speed=null;



    public Alternative alternative=null;

    public PhenomenonType type=null;

    public Phenomenon() {

    }

    public Phenomenon setType(PhenomenonType type) {
        this.type=type;
        return this;
    }

    public PhenomenonType getType() {
        return type==null?PhenomenonType.CONSTANT_STATIC:type;
    }

    public double getValue() {
        return value == null ? 1.0 : value;
    }

    public Phenomenon setValue(double value) {
        this.value = value;
        return this;
    }

    public double getActualValue(int step) {
        if (alternative != null && step >= alternative.getStep()) return alternative.getValue();
        return getValue();
    }

    public Phenomenon setAlternative(Alternative alternative) {
        this.alternative = alternative;
        return this;
    }

    public Alternative getAlternative() {
        return alternative == null ? new Alternative() : alternative;
    }

    public Phenomenon setRangeFrom(double rangeFrom) {
        this.rangeFrom=rangeFrom;
        return this;
    }

    public double getRangeFrom() {
        return rangeFrom==null?0:rangeFrom;
    }

    public Phenomenon setRangeTo(double rangeTo) {
        this.rangeTo=rangeTo;
        return this;
    }

    public double getRangeTo() {
        return rangeTo==null?1:rangeTo;
    }

    public Phenomenon setSpeed(double speed) {
        this.speed=speed;
        return this;
    }

    public double getSpeed() {
        return speed==null?0:speed;
    }

    public Phenomenon setAtZeroX(double atZeroX) {
        this.atZeroX=atZeroX;
        return this;
    }

    public double getAtZeroX() {
        return atZeroX==null?0:atZeroX;
    }

    public Phenomenon setAtZeroY(double atZeroY) {
        this.atZeroY=atZeroY;
        return this;
    }

    public double getAtZeroY() {
        return atZeroY==null?0:atZeroY;
    }

    public Phenomenon setIncrementX(double incrementX) {
        this.incrementX=incrementX;
        return this;
    }

    public double getIncrementX() {
        return incrementX==null?0:incrementX;
    }

    public Phenomenon setIncrementY(double incrementY) {
        this.incrementY=incrementY;
        return this;
    }

    public double getIncrementY() {
        return incrementY==null?0:incrementY;
    }

}

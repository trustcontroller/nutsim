package com.portifacto.nutsim.parameters;

import com.portifacto.nutsim.common.Belief;
import org.nd4j.shade.jackson.annotation.JsonAutoDetect;
import org.nd4j.shade.jackson.annotation.JsonIgnoreProperties;
import org.nd4j.shade.jackson.annotation.JsonInclude;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonIgnoreProperties(ignoreUnknown = true)
@JsonAutoDetect(getterVisibility = JsonAutoDetect.Visibility.NONE, setterVisibility = JsonAutoDetect.Visibility.NONE)
public class Josang {

    private static final Belief DEFAULT_VALUE = Belief.UNCERTAINTY;
    private static final boolean DEFAULT_RECALCULATE = false;

    public Belief value = null;
    public Boolean recalculate = null;

    public Josang() {

    }

    public Josang setValue(Belief value) {
        this.value = value;
        return this;
    }

    public Josang setRecalculate(boolean recalculate) {
        this.recalculate = recalculate;
        return this;
    }

    public Belief getValue() {
        return value == null ? DEFAULT_VALUE : value;
    }

    public boolean getRecalculate() {
        return recalculate == null ? DEFAULT_RECALCULATE : recalculate;
    }


}

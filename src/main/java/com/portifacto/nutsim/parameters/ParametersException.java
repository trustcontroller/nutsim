package com.portifacto.nutsim.parameters;

public class ParametersException extends Throwable {

    public ParametersException(String description) {
        super(description);
    }

    public ParametersException() {

    }

}

package com.portifacto.nutsim.parameters;

import org.nd4j.shade.jackson.annotation.JsonAutoDetect;
import org.nd4j.shade.jackson.annotation.JsonIgnoreProperties;
import org.nd4j.shade.jackson.annotation.JsonInclude;

import java.util.Random;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonIgnoreProperties(ignoreUnknown = true)
@JsonAutoDetect(getterVisibility = JsonAutoDetect.Visibility.NONE, setterVisibility = JsonAutoDetect.Visibility.NONE)
public class Role {

    // TODO: remove authority, introduce confidence and trustworthiness

    private static final Random rand = new Random();

    private static final double MEAN = 1.0;
    private static final double DEVIATION = 0.0;
    private final static double DRIFT = 0.0;

    public RoleLabel label = null;

    public Double mean = null;

    public Double deviation = null;

    public Double fraction = null;

    public Double drift = null;

    public Alternative alternative = null;

    public Integer count = null;

    public Confidence confidence = null;

    public Trustworthiness trustworthiness = null;

    public Boolean tracking = null;

    public Integer distortionFrom=null;

    public Integer distortionTo=null;

    public Josang josang=null;

    public Role() {

    }

    public Role(RoleLabel label, double mean, double deviation, Trustworthiness trustworthiness, Confidence confidence) {
        this.label = label;
        this.mean = mean;
        this.deviation = deviation;
        this.trustworthiness = trustworthiness;
        this.confidence = confidence;

    }

    public Role setJosang(Josang josang) {
        this.josang=josang;
        return this;
    }

    public Josang getJosang() {
        return josang==null?new Josang():josang;
    }

    public RoleLabel getLabel() { // TODO: add step
        if (label == null) return RoleLabel.NONE;
        return label;
    }

    public double getMean(int step) { //
        if (alternative != null && step >= alternative.getStep()) {
            return alternative.getValue();
        }
        if (mean == null) return MEAN;
        return mean;
    }

    public double getDeviation(int step) {
        if (alternative != null && step >= alternative.getStep()) {
            return alternative.getDeviation();
        }
        if (deviation == null) return DEVIATION;
        return deviation;
    }

    public double getFraction() {
        if (fraction == null) return 0.0;
        return fraction;
    }

    public int getCount() {
        if (count == null) return 0;
        return count;
    }

    public Trustworthiness getTrustworthiness(int step) {
        if (alternative != null && step >= alternative.getStep()) {
            return alternative.getTrustworthiness();
        }
        if (trustworthiness == null) return new Trustworthiness();
        return trustworthiness;
    }

    public Role setLabel(RoleLabel roleLabel) {
        this.label = roleLabel;
        return this;
    }

    public Role setMean(double mean) {
        this.mean = mean;
        return this;
    }

    public Role setDeviation(double deviation) {
        this.deviation = deviation;
        return this;
    }

    public Role setFraction(double fraction) {
        this.fraction = fraction;
        return this;
    }

    public Role setCount(int count) {
        this.count = count;
        return this;
    }

    public Role setTrustworthiness(Trustworthiness trustworthiness) {
        this.trustworthiness = trustworthiness;
        return this;
    }

    public Role setTracking(boolean tracking) {
        this.tracking = tracking;
        return this;
    }

    public boolean getTracking() {
        return tracking == null ? false : tracking;
    }

    public double generateMeasurement(int step, Phenomenon phenomenon) {
        if (getTracking()) {
            return phenomenon.getActualValue(step);
        }
        double v=getMean(step);
        if(step>=getDistortionFrom() && step<=getDistortionTo()) {
            double random = rand.nextGaussian();
            v += random * getDeviation(step);
            v += getDrift(step) * (step-getDistortionFrom());
            v = Math.min(v, 1.0);
        }
        // normalize to 0..1
        double value = Math.max(Math.min(v, 1.0), 0.0);
        return value;
    }

    public Role setConfidence(Confidence confidence) {
        this.confidence = confidence;
        return this;
    }

    public Confidence getConfidence(int step) {
        if (alternative != null && step >= alternative.getStep()) {
            return alternative.getConfidence();
        }
        if (confidence == null) return new Confidence();
        return confidence;
    }

    public Role setDrift(double drift) {
        this.drift = drift;
        return this;
    }

    public double getDrift(int step) {
        if (alternative != null && step >= alternative.getStep()) {
            return alternative.getDrift();
        }
        return drift == null ? 0.0 : drift;
    }

    public Role setAlternative(Alternative alternative) {
        this.alternative = alternative;
        return this;
    }

    public Alternative getAlternative() {
        if (alternative == null) return new Alternative();
        return alternative;
    }

    public Role setDistortionFrom(int distortionFrom) {
        this.distortionFrom=distortionFrom;
        return this;
    }

    public int getDistortionFrom() {
        return distortionFrom==null?Integer.MAX_VALUE:distortionFrom;
    }

    public Role setDistortionTo(int distortionTo) {
        this.distortionTo=distortionTo;
        return this;
    }

    public int getDistortionTo() {
        return distortionTo==null?0:distortionTo;
    }


}

package com.portifacto.nutsim.parameters;

public enum PhenomenonType {

    CONSTANT_STATIC,  // equals 'value' everywhere
    GRADIENT_STATIC,  // incline gradient from zero to one in the range, below the range: zero, above: one
    WAVE_STATIC,      // sine wave from zero to one, one wave /\/ int he range
    WAVE_DYNAMIC;     // sine wave one cycle within the range, moving at a defined speed
}

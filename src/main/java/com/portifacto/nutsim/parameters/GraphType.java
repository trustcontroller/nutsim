package com.portifacto.nutsim.parameters;

public enum GraphType {
    RANDOM,
    CLUSTER,
    DEFINED

}

package com.portifacto.nutsim.parameters;

/**
 * The 'parameters' package contains all the classes that allow for the construction
 * of the Parameters object. The object can be constructed by deserializing the
 * JSON file, or programmatically.
 * <p>
 * The Parameters object is immutable, i.e. once created, its content cannot be changed.
 * THe Playbook that takes Parameters creates its own structure that complements
 * Parameters.
 */
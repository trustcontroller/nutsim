package com.portifacto.nutsim.parameters;

public enum FusionType {
    CONSENSUS,
    TRANSITIVITY,
    REPUTATION,
    QUADRATIC,
    WEIGHTED;
}

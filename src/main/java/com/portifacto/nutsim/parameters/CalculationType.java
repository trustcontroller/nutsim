package com.portifacto.nutsim.parameters;

public enum CalculationType {

    BINARY,
    LINEAR,
    EXPONENTIAL,
    NONE,
    FULL,
    UNKNOWN
}

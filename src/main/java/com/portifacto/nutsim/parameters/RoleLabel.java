package com.portifacto.nutsim.parameters;

import org.nd4j.shade.jackson.annotation.JsonAutoDetect;
import org.nd4j.shade.jackson.annotation.JsonIgnoreProperties;
import org.nd4j.shade.jackson.annotation.JsonInclude;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonIgnoreProperties(ignoreUnknown = true)
@JsonAutoDetect(getterVisibility = JsonAutoDetect.Visibility.NONE, setterVisibility = JsonAutoDetect.Visibility.NONE)
public enum RoleLabel {
    TRUSTWORTHY,
    FAULTY,
    REFERENCE, //  high quality
    NORMAL, // good, but normal
    FOCUSED,// Gaussian, low dispersion, high shift
    EXACT, // Gaussian, high dispersion, low shift
    MEDIOCRE, // Gaussian, high dispersion, high shift
    ALTERNATIVE, // changes to alternative behaviour at some point
    TRACKER, // faithfully tracks the value of the phenomenon
    NONE;
}

package com.portifacto.nutsim.parameters;

import org.nd4j.shade.jackson.annotation.JsonAutoDetect;
import org.nd4j.shade.jackson.annotation.JsonIgnoreProperties;
import org.nd4j.shade.jackson.annotation.JsonInclude;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonIgnoreProperties(ignoreUnknown = true)
@JsonAutoDetect(getterVisibility = JsonAutoDetect.Visibility.NONE, setterVisibility = JsonAutoDetect.Visibility.NONE)
public class EdgeConstraint {

    public Integer source = null;
    public Integer destination = null;
    public Double range = null;

    public EdgeConstraint() {

    }

    public EdgeConstraint(Double range, Integer source, Integer destination) {
        this.range = range;
        this.source = source;
        this.destination = destination;
    }

    public Double getRange() {
        return range;
    }

    public Integer getSource() {
        return source;
    }

    public Integer getDestination() {
        return destination;
    }

    public boolean matches(int source, int destination) {
        if (this.source != null && this.source == source &&
                this.destination != null && this.destination == destination) return true;
        if (this.source == null &&
                this.destination != null && this.destination == destination) return true;
        if (this.source != null && this.source == source &&
                this.destination == null) return true;
        if (this.source == null && this.destination == null) return true;

        return false;
    }

    @Override
    public boolean equals(Object alter) {
        if (!(alter instanceof EdgeConstraint)) return false;
        return this.source == ((EdgeConstraint) alter).source &&
                this.destination == ((EdgeConstraint) alter).destination;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(source);
        sb.append("->");
        sb.append(destination);
        sb.append(": ");
        sb.append(range);
        return sb.toString();

    }

}





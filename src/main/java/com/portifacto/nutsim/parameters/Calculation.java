package com.portifacto.nutsim.parameters;

import org.nd4j.shade.jackson.annotation.JsonAutoDetect;
import org.nd4j.shade.jackson.annotation.JsonIgnoreProperties;
import org.nd4j.shade.jackson.annotation.JsonInclude;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonIgnoreProperties(ignoreUnknown = true)
@JsonAutoDetect(getterVisibility = JsonAutoDetect.Visibility.NONE, setterVisibility = JsonAutoDetect.Visibility.NONE)
public class Calculation {

    public CalculationType type = null;

    public Double threshold = null;

    public Double lambda = null;

    public Double slope = null;

    public CalculationType getType() {
        return type == null ? CalculationType.UNKNOWN : type;

    }

    public double getThreshold() {
        return threshold == null ? 0.0 : threshold;
    }

    public double getLambda() {
        return lambda == null ? 1.0 : lambda;
    }

    public double getSlope() {
        return slope == null ? 0.0 : slope;
    }

    public Calculation setType(CalculationType type) {
        this.type = type;
        return this;
    }

    public Calculation setThreshold(double threshold) {
        this.threshold = threshold;
        return this;
    }

    public Calculation setLambda(double lambda) {
        this.lambda = lambda;
        return this;
    }

    public Calculation setSlope(double slope) {
        this.slope = slope;
        return this;
    }

    public double calculateTrustworthiness(double expected, double actual) {
        double diff = Math.abs(expected - actual);

        switch (getType()) {
            case BINARY:
                return diff <= getThreshold() ? 1.0 : 0.0;
            case LINEAR:
                return Math.max(1.0 - Math.abs(getSlope()) * diff, 0);
            case EXPONENTIAL:
                return Math.exp(-Math.abs(getLambda()) * diff);
            case NONE:
                return 0.0;
            case FULL:
                return 1.0;
            case UNKNOWN:
                return 0.5;
        }
        return 0.0;
    }


}

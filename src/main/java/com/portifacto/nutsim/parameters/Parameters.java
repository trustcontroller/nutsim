package com.portifacto.nutsim.parameters;

import org.nd4j.shade.jackson.annotation.JsonAutoDetect;
import org.nd4j.shade.jackson.annotation.JsonIgnoreProperties;
import org.nd4j.shade.jackson.annotation.JsonInclude;
import org.nd4j.shade.jackson.databind.ObjectMapper;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonIgnoreProperties(ignoreUnknown = true)
@JsonAutoDetect(getterVisibility = JsonAutoDetect.Visibility.NONE, setterVisibility = JsonAutoDetect.Visibility.NONE)
// parameters are for the playbook and for the script, script overrides playbook, defaults
// are used if neither defined
public class Parameters {

    private static final String TITLE = "default";

    private static final int RUNS = 1;
    private static final int STEPS = 1;

    private static final double DECAY = 0.5;

    private static final GraphType GRAPH_TYPE = GraphType.DEFINED;
    private static final double DENSITY = 1.0;

    public String title = null;

    public Integer runs = null;
    public Integer steps = null;

    public Double decay = null;
    public GraphType graphType = null;

    public List<EdgeConstraint> constraints = null;

    public Calculation calculation = null;

    public Integer size = null; // number of nodes
    public Double density = null; // avg edges per node

    public Phenomenon phenomenon = null;

    public Fusion fusion =null;

    public List<Role> role = null;

    public Parameters() {

    }

    public static Parameters fromFile(File file) throws ParametersException {
        Parameters parameters = null;
        try {
            ObjectMapper objectMapper = new ObjectMapper();
            parameters = objectMapper.readValue(file, Parameters.class);
        } catch (Exception e) {
            throw new ParametersException(e.toString());
        }
        return parameters;
    }

    public Parameters setTitle(String title) {
        this.title = title;
        return this;
    }

    public String getTitle() {
        return title == null ? TITLE : title;
    }

    public Parameters setRuns(int runs) {
        this.runs = runs;
        return this;
    }

    public int getRuns() {
        if (runs != null) return runs;
        return RUNS;
    }

    public Parameters setSteps(int steps) {
        this.steps = steps;
        return this;
    }

    public int getSteps() {
        if (steps != null) return steps;
        return STEPS;
    }

    public double getDecay() {
        if (decay != null) return decay;
        return DECAY;
    }

    public Parameters setDecay(double decay) {
        this.decay = decay;
        return this;
    }

    public Parameters setGraphType(GraphType graphType) {
        this.graphType = graphType;
        return this;
    }

    public GraphType getGraphType() {
        if (graphType != null) return graphType;
        return GRAPH_TYPE;
    }

    public Parameters addConstraint(EdgeConstraint edgeConstraint) {
        if (constraints == null) {
            constraints = new ArrayList<>();
        }
        constraints.add(edgeConstraint);
        return this;
    }

    public List<EdgeConstraint> getConstraints() {
        return constraints;
    }

    public Double getConstraintValue(int source, int destination) {
        Double result = null;
        if (constraints == null) return null;
        for (EdgeConstraint constraint : constraints) {
            if (constraint.matches(source, destination)) {
                result = constraint.getRange();
            }
        }
        return result;
    }

    public Parameters setFusion(Fusion fusion) {
        this.fusion = fusion;
        return this;
    }

    public Fusion getFusion() {
        return fusion ==null?new Fusion(): fusion;
    }

    public List<Role> getRoles() {
        if (role == null) return new ArrayList<>();
        return role;
    }

    public Parameters addRole(Role newRole) {
        if (role == null) role = new ArrayList<>();
        role.add(newRole);
        return this;
    }

    public Parameters setSize(int size) {
        this.size = size;
        return this;
    }

    public int getSize() {
        if (size == null) return 0;
        return size;
    }

    public Calculation getCalculation() {
        return calculation == null ? new Calculation() : calculation;
    }

    public Parameters setCalculation(Calculation calculation) {
        this.calculation = calculation;
        return this;
    }

    public Parameters setDensity(double density) {
        this.density = density;
        return this;
    }

    public double getDensity() {
        return density == null ? DENSITY : density;
    }

    public Parameters setPhenomenon(Phenomenon phenomenon) {
        this.phenomenon = phenomenon;
        return this;
    }

    public Phenomenon getPhenomenon() {
        if (phenomenon == null) return new Phenomenon();
        return phenomenon;
    }


}

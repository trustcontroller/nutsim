package com.portifacto.nutsim.playbook;

import com.portifacto.nutsim.parameters.EdgeConstraint;

import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

public class EdgeConstraintGenerator {

    private final int size;
    private final IDistributor distributor;

    public EdgeConstraintGenerator(IDistributor distributor, int size) {
        this.size = size;
        this.distributor = distributor;
    }

    public List<EdgeConstraint> generate() {
        return IntStream
                .range(0, size)
                .mapToObj(source ->
                        IntStream
                                .range(0, size)
                                .mapToObj(destination ->
                                        new EdgeConstraint(source == destination ? 0 :
                                                distributor.nextDouble(source, destination),
                                                source,
                                                destination))
                ).flatMap(constraint -> constraint)
                .collect(Collectors.toList());
    }
}

package com.portifacto.nutsim.playbook;

import com.portifacto.nutsim.parameters.Parameters;
import com.portifacto.nutsim.parameters.Role;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

public class PlaybookScript {

    private static class CounterPerRole {
        private Role role;
        private int count;

        public CounterPerRole(Role role, int count) {
            this.role = role;
            this.count = count;
        }

        public int getCount() {
            return count;
        }

        public Role getRole() {
            return role;
        }

    }

    private String name = null; // this may be the generated name "Run x"

    private List<PlaybookStep> timeline = new ArrayList<>();

    private Playbook playbook = null;

    public static PlaybookScript fromParameters(Playbook playbook, String name) {
        PlaybookScript script = new PlaybookScript();
        script.name = name;
        script.playbook = playbook;
        Parameters parameters = playbook.getParameters();

        // calculate the total and relative number of nodes for each role
        List<Role> roles = parameters.getRoles();
        double total = 0;
        for (Role role : roles) {
            total += role.getFraction();
        }

        List<CounterPerRole> counterPerRoles = new ArrayList<>();
        List<Role> templates = new ArrayList<>();
        if (!roles.isEmpty()) {
            int deployed = 0;
            for (Role role : roles) {
                int count = (int) ((role.getFraction()) * (((double) parameters.getSize()) / total));
                deployed += count;
                counterPerRoles.add(new CounterPerRole(role, count));

            }

            if (deployed < parameters.getSize()) {
                int diff = parameters.getSize() - deployed;
                counterPerRoles.add(new CounterPerRole(roles.get(0), diff));
            }

            // create initial template list of roles with initial roles
            for (CounterPerRole counterPerRole : counterPerRoles) {
                for (int i = 0; i < counterPerRole.getCount(); i++) {
                    templates.add(counterPerRole.getRole());
                }
            }
        } else {
            // strange situation when no role is defined, set all to default (NONE)
            for (int i = 0; i < playbook.getSize(); i++) {
                templates.add(new Role());
            }
        }

        // define the maximum number of edges, never more than allowed by the logic of the graph
        int edgeCount = (int) Math.min(parameters.getSize() * parameters.getDensity(),
                parameters.getSize() * (parameters.getSize() - 1));

        for (int step = 0; step < parameters.getSteps(); step++) {

            EdgeGenerator edgeGenerator = new EdgeGenerator(playbook);

            // assign values for nodes in this step according to their roles
            List<PlaybookNode> actualNodes = new ArrayList<>();
            int id = 0;
            for (Role role : templates) {
                actualNodes.add(new PlaybookNode(id++, role,
                        role.generateMeasurement(step, parameters.getPhenomenon()),
                        parameters.getPhenomenon().getActualValue(step)));
            }

            // construct edges for one step
            List<PlaybookOpinion> edgesForStep = new ArrayList<>();
            switch (parameters.getGraphType()) {
                case RANDOM:
                    edgesForStep = edgeGenerator.generateRandomOpinions(edgeCount, parameters, actualNodes);
                    break;
                // TODO: remaining ones: defined, cluster
            }

            // consolidate and save
            PlaybookStep playbookStep = new PlaybookStep(step, actualNodes, edgesForStep); // was: nodes
            script.timeline.add(playbookStep);
        }
        return script;
    }

    public String getName() {
        return name;
    }

    public List<PlaybookStep> getTimeline() {
        return timeline;
    }

    public Playbook getPlaybook() {
        return playbook;
    }

    // TODO: only for the schema
    void verify(Playbook playbook) throws PlaybookException {
        if (name == null) {
            throw new PlaybookException("Name must not be null");
        }

        int currentTime = 1;
        for (PlaybookStep step : timeline) {
            currentTime = step.verify(playbook, currentTime);
        }
        // sort timeline by increasing time, just in case
        Collections.sort(timeline, Comparator.comparing(PlaybookStep::getTime));

    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("  Script ");
        sb.append(getName());
        sb.append(" of ");
        sb.append(getTimeline().size());
        sb.append(" steps");
        sb.append('\n');
        for (PlaybookStep step : getTimeline()) {
            sb.append(step.toString());
        }
        return sb.toString();
    }

}

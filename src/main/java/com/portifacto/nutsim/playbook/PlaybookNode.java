package com.portifacto.nutsim.playbook;

import com.portifacto.nutsim.parameters.Role;

public class PlaybookNode {

    private final int id;
    private final Role role;
    private final double measured;
    private final double phenomenon;

    public PlaybookNode(int id, Role role, double measured, double phenomenon) {
        this.id = id;
        this.role = role;
        this.measured = measured;
        this.phenomenon = phenomenon;
    }


    public void verify(Playbook playbook) {

    }

    public int getId() {
        return id;
    }

    public double getMeasured() {
        return measured;
    }

    public double getPhenomenon() {
        return phenomenon;
    }

    public Role getRole() {
        return role;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("      Node ");
        sb.append(getId());
        sb.append(" ");
        sb.append(getRole().getLabel().name());
        sb.append('\n');
        return sb.toString();
    }


}

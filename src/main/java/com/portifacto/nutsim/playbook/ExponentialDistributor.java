package com.portifacto.nutsim.playbook;

import java.util.Random;

/**
 * Coefficient equals zero makes the distribution constant, coefficient equals one makes it even,
 * larger coefficients make it exponential; but always maps 0..1 to 0..1, always symmetric
 */
public class ExponentialDistributor implements IDistributor {

    private final double coefficient;

    private static Random random = new Random();

    public ExponentialDistributor(double coefficient) {
        this.coefficient = coefficient;
    }

    @Override
    public double nextDouble(int source, int destination) {
        return Math.pow(random.nextDouble(), coefficient);
    }

}

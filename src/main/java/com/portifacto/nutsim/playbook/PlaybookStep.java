package com.portifacto.nutsim.playbook;

import java.util.ArrayList;
import java.util.List;

public class PlaybookStep {

    public Integer time = null; // JSON
    public List<PlaybookNode> nodes = null; // JSON
    public List<PlaybookOpinion> opinions = new ArrayList<>();  // JSON

    public PlaybookStep() {

    }

    public PlaybookStep(int time, List<PlaybookNode> nodes, List<PlaybookOpinion> opinions) {
        this.time = time;
        this.nodes = nodes;
        this.opinions = opinions;
    }


    public Integer getTime() {
        return time;
    }

    public List<PlaybookNode> getNodes() {
        return nodes;
    }

    public List<PlaybookOpinion> getOpinions() {
        return opinions;

    }

    public int verify(Playbook playbook, int defaultTime) throws PlaybookException {
        if (nodes != null) {
            if (nodes.size() > playbook.getSize()) {
                throw new PlaybookException("Too many nodes in step " + defaultTime);
            }
            for (PlaybookNode node : nodes) {
                if (node != null) node.verify(playbook);
            }

            nodes = playbook.collateNodes(nodes);

        }
        int nextTime;
        if (time == null) {
            time = defaultTime;
            nextTime = defaultTime + 1;
        } else {
            nextTime = time.intValue() + 1;
        }

        // process and verify opinions, nodes should be in order
//        try {
//            if (opinions != null) {
//                int by = 0;
//                int about = 0;
//                for (PlaybookOpinion opinion : opinions) {
//                    PlaybookOpinion.OpinionResult result = opinion.verify(by, about, playbook.getSize());
//                    by = result.getSource();
//                    about = result.getDestination();
//                }
//            }
//        } catch (ParametersException e) {
//            throw new PlaybookException(e.getMessage());
//        }

        return nextTime;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("    Step at ");
        sb.append(getTime());
        sb.append('\n');
        for (PlaybookNode node : getNodes()) {
            sb.append(node.toString());
        }
        for (PlaybookOpinion opinion : getOpinions()) {
            sb.append(opinion.toString());
        }
        return sb.toString();
    }


}

package com.portifacto.nutsim.playbook;

import com.portifacto.nutsim.parameters.Parameters;
import com.portifacto.nutsim.schema.Script;
import org.nd4j.shade.jackson.core.util.DefaultPrettyPrinter;
import org.nd4j.shade.jackson.databind.ObjectMapper;

import java.io.File;
import java.util.ArrayList;
import java.util.List;


public class Playbook {

    private int size = 0;
    private Parameters parameters = null;
    public List<PlaybookScript> scripts = new ArrayList<>();

    private List<PlaybookNode> nodes = new ArrayList<>();

    public Playbook() {

    }

    public static Playbook fromParameters(Parameters parameters) throws PlaybookException {
        Playbook playbook = new Playbook();
        playbook.parameters = parameters;
        playbook.size = parameters.getSize();

        for (int run = 0; run < parameters.getRuns(); run++) {
            playbook.scripts.add(PlaybookScript.fromParameters(playbook, "Run " + run));
        }

        return playbook;
    }

    public static Playbook fromScript(Script script) {
        // TODO: used only in conjunction with a schema
        return new Playbook();
    }

    public Parameters getParameters() {
        return parameters;
    }

    public List<PlaybookScript> getScripts() {
        return scripts;
    }

    public String getTitle() {
        return parameters.getTitle();
    }

    public int getSize() {
        return size;
    }

    // TODO: verification is only for schema
    void verify() throws PlaybookException {
        if (size == 0) {
            throw new PlaybookException("Size must be greater than zero");
        }

        for (int i = 0; i < size; i++) {
            //nodes.add(PlaybookNode.fromParameters(this));
        }

        for (PlaybookScript s : scripts) {
            s.verify(this);
        }

    }

    // TODO: only for the schema, I think
    List<PlaybookNode> collateNodes(List<PlaybookNode> nodes) {
        List<PlaybookNode> updatedNodes = new ArrayList<>(this.nodes); // shallow copy
        for (int i = 0; i < nodes.size(); i++) {
            if (nodes.get(i) != null) {
                updatedNodes.set(i, nodes.get(i));
            }
        }
        this.nodes = updatedNodes;
        return this.nodes;

    }


    public void toFile(File file) throws PlaybookException {
        try {
            DefaultPrettyPrinter prettyPrinter = new DefaultPrettyPrinter();
//            prettyPrinter.indentArraysWith(DefaultIndenter.SYSTEM_LINEFEED_INSTANCE);
            ObjectMapper objectMapper = new ObjectMapper();
//            prettyPrinter.withArrayIndenter(DefaultIndenter.SYSTEM_LINEFEED_INSTANCE);
            objectMapper.writer(prettyPrinter).writeValue(file, this);
        } catch (Exception e) {
            throw new PlaybookException(e.toString());
        }

    }


    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("Playbook ");
        sb.append(getTitle());
        sb.append(" of size ");
        sb.append(getSize());
        sb.append('\n');
        for (PlaybookScript script : getScripts()) {
            sb.append(script.toString());
        }
        return sb.toString();
    }


}

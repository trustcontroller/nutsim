package com.portifacto.nutsim.playbook;

public interface IDistributor {

    public double nextDouble(int source, int destination);
}

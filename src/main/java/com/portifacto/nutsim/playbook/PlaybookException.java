package com.portifacto.nutsim.playbook;

public class PlaybookException extends Throwable {

    public PlaybookException(String description) {
        super(description);
    }

    public PlaybookException() {

    }
}

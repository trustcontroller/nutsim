package com.portifacto.nutsim.playbook;

import com.portifacto.nutsim.parameters.Parameters;

import java.util.*;

public class EdgeGenerator {

    private class Element implements Comparable<Element> {
        private double ceiling;
        private int source;
        private int destination;

        private Element(double ceiling, int source, int destination) {
            this.ceiling = ceiling;
            this.source = source;
            this.destination = destination;
        }

        public double getCeiling() {
            return ceiling;
        }

        public int getSource() {
            return source;
        }

        public int getDestination() {
            return destination;
        }

        @Override
        public boolean equals(Object alter) {
            if (!(alter instanceof Element)) return false;
            return this.source == ((Element) alter).source && this.destination == ((Element) alter).destination;
        }

        @Override
        public int compareTo(Element alter) {
            return this.ceiling < alter.ceiling ? -1 : this.ceiling > alter.ceiling ? 1 : 0;
        }
    }

    private static Random random = new Random();

    private double fullRange = 0;

    private NavigableSet<Element> mapping = new TreeSet<>();


    public EdgeGenerator(Playbook playbook) {
        Parameters parameters = playbook.getParameters();
        final int nodeCount = playbook.getSize();
        fullRange = 0;
        for (int source = 0; source < nodeCount; source++) {
            for (int destination = 0; destination < nodeCount; destination++) {
                double step = 1.0;
                Double value = parameters.getConstraintValue(source, destination);
                if (value != null) step = value;
                fullRange += step;
                mapping.add(new Element(fullRange, source, destination));
            }
        }

    }

    public PlaybookOpinion calculate(Parameters parameters, double value, List<PlaybookNode> nodes) {
        Element current = new Element(value, 0, 0);
        Element selected = mapping.ceiling(current);
        if (selected == null) return null;

        // resolve according to the policy
        double sourceValue = nodes.get(selected.getSource()).getMeasured();
        double destinationValue = nodes.get(selected.getDestination()).getMeasured();
        double opinionValue = parameters.getCalculation().calculateTrustworthiness(sourceValue, destinationValue);

//        return new PlaybookOpinion(selected.getSource(), selected.getDestination(),
//                1.0, opinionValue);

        return new PlaybookOpinion(selected.getSource(), selected.getDestination());
    }

    private PlaybookOpinion generateEdge(Parameters parameters, List<PlaybookNode> nodes) {
        PlaybookOpinion selected = null;
        while (selected == null) {
            double rand = random.nextDouble() * fullRange;
            selected = calculate(parameters, rand, nodes);
        }
        return selected;

    }

    public List<PlaybookOpinion> generateRandomOpinions(int edgeCount, Parameters parameters, List<PlaybookNode> nodes) {
        List<PlaybookOpinion> selected = new ArrayList<>(edgeCount);
        List<PlaybookOpinion> result = new ArrayList<>(edgeCount);
        for (int i = 0; i < edgeCount; i++) {
            while (true) {
                PlaybookOpinion edge = generateEdge(parameters, nodes);
                if (edge.getSource() == edge.getDestination()) continue;
                if (selected.contains(edge)) continue;
                result.add(edge);
                selected.add(edge);
                break;
            }
        }
        return result;
    }


}

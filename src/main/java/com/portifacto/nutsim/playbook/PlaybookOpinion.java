package com.portifacto.nutsim.playbook;


import com.portifacto.nutsim.schema.Opinion;

/**
 * This class holds opinions within the Playbook. Opinions MUST have source and
 * destination (i.e. they MUST define the edge), but they MAY have confidence
 * and trustworthiness defined. In general, if the playbook is created off
 * parameters, only the edge is defined; if it is created off the schema,
 * all components are defined.
 * <p>
 * The simulator is able to supplement all the missing values at the time
 * of simulation, using the combination of this object, parameters and some
 * default values.
 * <p>
 * The internal convention is that null means that the value is not defined.
 */
public class PlaybookOpinion implements Comparable<PlaybookOpinion> {

    static private final double DEFAULT_CONFIDENCE = 1.0;

    private final int source;
    private final int destination;
    private Double confidence = null;
    private Double value = null;

    public PlaybookOpinion(int source, int destination, double confidence, double value) {

        this.source = source;
        this.destination = destination;
        this.confidence = confidence;
        this.value = value;
    }

    public PlaybookOpinion(int source, int destination) {
        this.source = source;
        this.destination = destination;
    }

    public PlaybookOpinion(Opinion opinion) {
        this.source = opinion.getSource();
        this.destination = opinion.getDestination();
        this.confidence = opinion.getConfidence();
        this.value = opinion.getValue();
    }

    public int getSource() {
        return source;
    }

    public int getDestination() {
        return destination;
    }

    public PlaybookOpinion setConfidence(double confidence) {
        this.confidence = confidence;
        return this;
    }


    public boolean hasConfidence() {
        return confidence != null;
    }

    public double getConfidence() {
        return confidence == null ? 0 : confidence;
    }

    public PlaybookOpinion setValue(double value) {
        this.value = value;
        return this;
    }

    public boolean hasValue() {
        return value != null;
    }

    public double getValue() {
        return value == null ? 0 : value;
    }

    @Override
    public boolean equals(Object alter) {
        if (!(alter instanceof PlaybookOpinion)) {
            return false;
        }
        return this.source == ((PlaybookOpinion) alter).source &&
                this.destination == ((PlaybookOpinion) alter).destination;
    }

    @Override
    public int compareTo(PlaybookOpinion ego) {
        if (!(this.source == ego.source)) return this.source - ego.source;
        if (!(this.destination == ego.destination)) return this.destination - ego.destination;
        return 0;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("      Opinion ");
        sb.append(getSource());
        sb.append(" -> ");
        sb.append(getDestination());
        sb.append(" ");
        if (hasValue()) sb.append(String.format("%.2f", getValue()));
        else sb.append('-');
        sb.append(" @ ");
        if (hasConfidence()) sb.append(String.format("%.2f", getConfidence()));
        else sb.append('-');
        sb.append('\n');
        return sb.toString();
    }


}

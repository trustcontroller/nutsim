package com.portifacto.nutsim.playbook;

import com.portifacto.nutsim.parameters.Phenomenon;

public class PlaybookPhenomenon {

    Phenomenon phenomenon;

    public PlaybookPhenomenon(Phenomenon phenomenon) {
        this.phenomenon = phenomenon;
    }

    public double getActualValue(int step) {
        return phenomenon.getActualValue(step);
    }
}

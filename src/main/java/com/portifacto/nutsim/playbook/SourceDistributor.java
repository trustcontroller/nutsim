package com.portifacto.nutsim.playbook;

import java.util.List;
import java.util.Random;

/**
 * Coefficient equals zero makes the distribution constant, coefficient equals one makes it even,
 * larger coefficients make it exponential; but always maps 0..1 to 0..1, always symmetric
 */
public class SourceDistributor implements IDistributor {

    private final List<Integer> nodes;
    private final double preferential;
    private final double others;

    private static final Random random = new Random();


    public SourceDistributor(List<Integer> nodes, double preferential, double others) {
        this.nodes = nodes;
        this.preferential = preferential;
        this.others = others;
    }

    @Override
    public double nextDouble(int source, int destination) {
        if (nodes.contains(source)) {
            return preferential;
        } else {
            return others;
        }

    }

}

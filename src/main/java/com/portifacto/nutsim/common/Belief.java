package com.portifacto.nutsim.common;

/**
 * Implements Josang's opinions from the logic for uncertain probabilities
 * (DOI https://doi.org/10.1142/S0218488501000831), with operations
 * as defined in DOI: 10.1016/j.ijar.2004.03.003
 */
public class Belief {

    // this is the default value of atomicity relative to all the opinions and reputations
    // as their number is quite large (and vary), one can assume that the atomicity of an individual
    // evidence is close to zero
    private static final double DEFAULT_ATOMICITY = 0.0;

    private static final double EPSILON = 0.001;

    private final boolean valid;

    private final double belief;
    private final double disbelief;
    private final double uncertainty;
    private final double atomicity;

    static public final Belief BELIEF = Belief.of(1, 0, 0);
    static public final Belief DISBELIEF = Belief.of(0, 1, 0);
    static public final Belief UNCERTAINTY = Belief.of(0, 0, 1);
    static public final Belief INVALID = new Belief();

    private Belief(double belief, double disbelief, double uncertainty, double atomicity) {
        // normalise by proportionally increasing or decreasing values
        double multiplier=1.0/(belief + disbelief + uncertainty);
        this.belief = belief*multiplier;
        this.disbelief = disbelief*multiplier;
        this.uncertainty = uncertainty*multiplier;
        this.atomicity = atomicity;
        this.valid = ((belief + disbelief + uncertainty) <= (1 + EPSILON) &&
                (belief + disbelief + uncertainty) >= (1 - EPSILON) &&
                belief >= -EPSILON && belief <= (1 + EPSILON) &&
                disbelief >= -EPSILON && disbelief <= (1 + EPSILON) &&
                uncertainty >= -EPSILON && uncertainty <= (1 + EPSILON) &&
                atomicity >= -EPSILON && atomicity <= (1 + EPSILON));
    }

    private Belief() {
        this.valid = false;
        this.belief = 0;
        this.disbelief = 0;
        this.uncertainty = 1;
        this.atomicity = DEFAULT_ATOMICITY;
    }

    static public Belief of(double belief, double disbelief, double uncertainty) {
        return new Belief(belief, disbelief, uncertainty, DEFAULT_ATOMICITY);
    }

    static public Belief of(double belief, double disbelief, double uncertainty, double atomicity) {
        return new Belief(belief, disbelief, uncertainty, atomicity);
    }

    static public Belief ofEU(double expectation, double uncertainty) {
        if (expectation < -EPSILON || expectation > 1+EPSILON || uncertainty < -EPSILON || uncertainty > 1+EPSILON) {
            return INVALID;
        }
        if (uncertainty < EPSILON) {
            return new Belief(expectation, 1 - expectation, 0, expectation);
        }
        if (uncertainty > 1 - EPSILON) {
            return new Belief(0, 0, 1, expectation);
        }
        final double y = (1 - uncertainty) / 2;
        final double x = (expectation - 0.5) * (1 - uncertainty);
        final double belief = (x + y) * (1 - uncertainty) / (2 * y);
        final double disbelief = 1 - belief - uncertainty;
        final double atomicity = (expectation - belief) / uncertainty;
        return new Belief(belief, disbelief, uncertainty, atomicity);
    }

    public boolean isValid() {
        return valid;
    }

    public double getBelief() {
        if (!valid) return Double.NaN;
        return belief;
    }

    public double getDisbelief() {
        if (!valid) return Double.NaN;
        return disbelief;
    }

    public double getUncertainty() {
        if (!valid) return Double.NaN;
        return uncertainty;
    }

    public double getAtomicity() {
        if (!valid)
            return Double.NaN;
        return atomicity;
    }

    public double getExpectation() {
        if (!valid) {
            return Double.NaN;}
        return belief + atomicity * uncertainty;
    }

    public Belief consensus(Belief b) {
        if (b == null) return Belief.INVALID;
        if (!valid || !b.valid) return Belief.INVALID;
        double k = uncertainty + b.uncertainty - (uncertainty * b.uncertainty);
        double blf = (belief * b.uncertainty + b.belief * uncertainty) / k;
        double dbf = (disbelief * b.uncertainty + b.disbelief * uncertainty) / k;
        double unc = (uncertainty * b.uncertainty) / k;
        double atm;
        final double ax=uncertainty + b.uncertainty - 2 * uncertainty * b.uncertainty;
        if (ax <= EPSILON) {
            blf = 0;
            dbf = 0;
            unc = 1;
            atm = 0.5; // default value
        } else {
            atm = (b.atomicity * uncertainty +
                    atomicity * b.uncertainty -
                    (atomicity + b.atomicity) * uncertainty * b.uncertainty) /
                    ax;
        }
        return Belief.of(blf, dbf, unc, atm);
    }

    public Belief discounting(Belief b) {
        if (b == null) return Belief.INVALID;
        if (!valid || !b.valid) return Belief.INVALID;
        double blf = belief * b.belief;
        double dbf = belief * b.disbelief;
        double unc = disbelief + uncertainty + belief * b.uncertainty;
        double atm = b.atomicity;
        return Belief.of(blf, dbf, unc, atm);
    }

    // low numbers imply slow decay of certainty (slow increase in uncertainty)
    // high numbers works the other way
    // e.g. for initial uncertainty=0.2
    // for decay factor = 0.2 we have the sequence of uncertainties
    // 0.2, 0.36, 0.488 etc.
    // while for decay factor 0.4 we have 0.2, 0.52, 0.712 etc.
    public Belief decay(double factor) {
        if (factor < 0 || factor > 1) return INVALID;
        double u = 1.0 - ((1.0 - uncertainty) * (1-factor));
        double delta = u - uncertainty;
        double d;
        double b;
        if (delta < EPSILON) {
            b = belief;
            d = disbelief;
        } else if ((belief + disbelief) < EPSILON) {
            b = 0;
            d = 0;
        } else {
            b = belief - (belief / (belief + disbelief) * delta);
            d = disbelief - (disbelief / (belief + disbelief) * delta);
        }
        return new Belief(b, d, u, atomicity);
    }

    @Override
    public boolean equals(Object alter) {
        if (!(alter instanceof Belief)) return false;
        return (belief >= ((Belief) alter).belief - EPSILON &&
                belief <= ((Belief) alter).belief + EPSILON &&
                disbelief >= ((Belief) alter).disbelief - EPSILON &&
                disbelief <= ((Belief) alter).disbelief + EPSILON &&
                uncertainty >= ((Belief) alter).uncertainty - EPSILON &&
                uncertainty <= ((Belief) alter).uncertainty + EPSILON &&
                atomicity >= ((Belief) alter).atomicity - EPSILON &&
                atomicity <= ((Belief) alter).atomicity + EPSILON);
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        if (!valid) sb.append('!');
        sb.append('[');
        sb.append(String.format("%.4f", belief));
        sb.append(',');
        sb.append(String.format("%.4f", disbelief));
        sb.append(',');
        sb.append(String.format("%.4f", uncertainty));
        sb.append(',');
        sb.append(String.format("%.4f", atomicity));
        sb.append(']');
        return sb.toString();
    }


}

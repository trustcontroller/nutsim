package com.portifacto.nutsim.report;

import com.portifacto.nutsim.common.Belief;
import com.portifacto.nutsim.simulation.SimulationOpinion;

import java.util.ArrayList;
import java.util.List;

public class NodeResult {

    private final int id;
    private final double phenomenon;
    private final double measured;
    private final double trustworthiness;
    private final double confidence;
    private final double weight;
    private final Belief belief;
    private final List<OpinionResult> opinions;


    public NodeResult(int id, double phenomenon, double measured, double trustworthiness,
                      double confidence,double weight,Belief belief, List<SimulationOpinion> opinions) {
        this.id = id;
        this.phenomenon = phenomenon;
        this.measured = measured;
        this.trustworthiness = trustworthiness;
        this.confidence = confidence;
        this.belief = belief;
        this.weight=weight;
        this.opinions = new ArrayList<>();
        if (opinions != null) {
            for (SimulationOpinion simulationOpinion : opinions) {
                this.opinions.add(new OpinionResult(simulationOpinion));
            }
        }
    }

    public int getId() {
        return id;
    }

    public double getTrustworthiness() {
        return trustworthiness;
    }

    public double getConfidence() {
        return confidence;
    }

    public Belief getBelief() {
        return belief;
    }

    public double getPhenomenon() {
        return phenomenon;
    }

    public double getMeasured() {
        return measured;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("      Node ");
        sb.append(id);
        sb.append(", trustworthiness: ");
        sb.append(String.format("%.3f", trustworthiness));
        sb.append(", weight: ");
        sb.append(String.format("%.3f", weight));
        if (belief != null) {
            sb.append(", belief: ");
            sb.append(belief.toString());
        }
        sb.append(" measured: ");
        sb.append(String.format("%.3f", measured));
        sb.append(" phenomenon: ");
        sb.append(String.format("%.3f", phenomenon));
        sb.append('\n');
        for (OpinionResult opinionResult : opinions) {
            sb.append(opinionResult.toString());
        }
        return sb.toString();
    }
}

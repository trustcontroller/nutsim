package com.portifacto.nutsim.report;

import java.util.ArrayList;
import java.util.List;

public class StepResult {

    private List<NodeResult> nodeResults = new ArrayList<>();

    private int time;

    public StepResult(int time) {
        this.time = time;
    }

    public StepResult appendResult(NodeResult nodeResult) {
        nodeResults.add(nodeResult);
        return this;
    }

    public StepResult appendResult(List<NodeResult> nodeResultList) {
        nodeResults.addAll(nodeResultList);
        return this;
    }

    public int getTime() {
        return time;
    }

    public List<NodeResult> getNodeResults() {
        return nodeResults;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("    Step at ");
        sb.append(getTime());
        sb.append('\n');
        int node = 0;
        for (NodeResult nodeResult : nodeResults) {
            sb.append(nodeResult.toString());
            node++;
        }
        return sb.toString();
    }

}

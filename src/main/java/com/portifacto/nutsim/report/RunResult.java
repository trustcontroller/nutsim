package com.portifacto.nutsim.report;

import com.portifacto.nutsim.playbook.PlaybookNode;
import com.portifacto.nutsim.playbook.PlaybookScript;

import java.util.ArrayList;
import java.util.List;

public class RunResult {

    private List<StepResult> results = new ArrayList<>();

    private PlaybookScript script;

    private List<PlaybookNode> nodes;

    public RunResult(PlaybookScript script) {
        this.script = script;
    }

    public RunResult appendStep(StepResult stepResult) {
        results.add(stepResult);
        return this;
    }

    public RunResult appendNodes(List<PlaybookNode> nodes) {
        this.nodes = nodes;
        return this;
    }

    public List<StepResult> getResults() {
        return results;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("  Run ");
        sb.append('\n');
        for (StepResult stepResult : results) {
            sb.append(stepResult.toString());
        }
        return sb.toString();
    }

}

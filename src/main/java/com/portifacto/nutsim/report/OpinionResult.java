package com.portifacto.nutsim.report;

import com.portifacto.nutsim.common.Belief;
import com.portifacto.nutsim.simulation.SimulationOpinion;

public class OpinionResult {

    private int source;
    private int destination;
    private double confidence;
    private double value;
    private double trustworthiness;
    private Belief belief;

    public OpinionResult(SimulationOpinion opinion) {
        this.source = opinion.getSource();
        this.destination = opinion.getDestination();
        this.confidence = opinion.getConfidence();
        this.value = opinion.getValue();
        this.trustworthiness =opinion.getTrustworthiness();
        this.belief=opinion.getBelief();
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("        Inbound from ");
        sb.append(source);
        sb.append(" value: ");
        sb.append(String.format("%.3f", value));
        sb.append(" confidence: ");
        sb.append(String.format("%.3f", confidence));
        sb.append(" trustworthiness: ");
        sb.append(String.format("%.3f", trustworthiness));
        sb.append(" opinion: ");
        sb.append(belief.toString());
        sb.append('\n');
        return sb.toString();
    }

}

package com.portifacto.nutsim.report;

import com.portifacto.nutsim.playbook.Playbook;

import java.util.ArrayList;
import java.util.List;

public class Report {

    private class DimensionedArray {
        public int runs;
        public int steps;
        public int nodes;
        public NodeResult[][][] standardArray;

        public DimensionedArray(int runs, int steps, int nodes,
                                NodeResult standardArray[][][]) {
            this.runs = runs;
            this.steps = steps;
            this.nodes = nodes;
            this.standardArray = standardArray;
        }

    }

    private List<RunResult> results = new ArrayList<>();

    private Playbook playbook;

    public Report(Playbook playbook) {
        this.playbook = playbook;
    }

    public List<RunResult> getHierarchicalOrder() {
        return results;
    }

    public Report appendRun(RunResult runResult) {
        results.add(runResult);
        return this;
    }

    public Playbook getPlaybook() {
        return playbook;
    }

    public int getMaxRunCount() {
        return results.size();

    }

    public int getMaxStepCount() {
        int steps = 0;
        for (RunResult runResult : results) {
            steps = Math.max(steps, runResult.getResults().size());
        }
        return steps;
    }

    public int getMaxNodeCount() {
        int nodes = 0;
        for (RunResult runResult : results) {
            for (StepResult stepResult : runResult.getResults()) {
                nodes = Math.max(nodes, stepResult.getNodeResults().size());
            }
        }
        return nodes;
    }

    /**
     * Run-Step-Node (i.e. 'generic')
     *
     * @return
     */
    public List<List<List<NodeResult>>> getRSNOrder() {
        DimensionedArray dimensionedArray = toDimensionedArray();
        List<List<List<NodeResult>>> level0 = new ArrayList<>();
        for (int run = 0; run < dimensionedArray.runs; run++) {
            List<List<NodeResult>> level1 = new ArrayList<>();
            for (int step = 0; step < dimensionedArray.steps; step++) {
                List<NodeResult> level2 = new ArrayList<>();
                for (int node = 0; node < dimensionedArray.nodes; node++) {
                    level2.add(dimensionedArray.standardArray[run][step][node]);
                }
                level1.add(level2);
            }
            level0.add(level1);
        }
        return level0;
    }

    /**
     * Run-Node-Step
     *
     * @return
     */
    public List<List<List<NodeResult>>> getRNSOrder() {
        DimensionedArray dimensionedArray = toDimensionedArray();
        List<List<List<NodeResult>>> level0 = new ArrayList<>();
        for (int run = 0; run < dimensionedArray.runs; run++) {
            List<List<NodeResult>> level1 = new ArrayList<>();
            for (int node = 0; node < dimensionedArray.nodes; node++) {
                List<NodeResult> level2 = new ArrayList<>();
                for (int step = 0; step < dimensionedArray.steps; step++) {
                    level2.add(dimensionedArray.standardArray[run][step][node]);
                }
                level1.add(level2);
            }
            level0.add(level1);
        }
        return level0;
    }

    /**
     * Step-Run-Node
     *
     * @return
     */
    public List<List<List<NodeResult>>> getSRNOrder() {
        DimensionedArray dimensionedArray = toDimensionedArray();
        List<List<List<NodeResult>>> level0 = new ArrayList<>();
        for (int step = 0; step < dimensionedArray.steps; step++) {
            List<List<NodeResult>> level1 = new ArrayList<>();
            for (int run = 0; run < dimensionedArray.runs; run++) {
                List<NodeResult> level2 = new ArrayList<>();
                for (int node = 0; node < dimensionedArray.nodes; node++) {
                    level2.add(dimensionedArray.standardArray[run][step][node]);
                }
                level1.add(level2);
            }
            level0.add(level1);
        }
        return level0;
    }

    /**
     * Step-Node-Run
     *
     * @return
     */
    public List<List<List<NodeResult>>> getSNROrder() {
        DimensionedArray dimensionedArray = toDimensionedArray();
        List<List<List<NodeResult>>> level0 = new ArrayList<>();
        for (int step = 0; step < dimensionedArray.steps; step++) {
            List<List<NodeResult>> level1 = new ArrayList<>();
            for (int node = 0; node < dimensionedArray.nodes; node++) {
                List<NodeResult> level2 = new ArrayList<>();
                for (int run = 0; run < dimensionedArray.runs; run++) {
                    level2.add(dimensionedArray.standardArray[run][step][node]);
                }
                level1.add(level2);
            }
            level0.add(level1);
        }
        return level0;
    }

    /**
     * Node-Run-Step
     *
     * @return
     */
    public List<List<List<NodeResult>>> getNRSOrder() {
        DimensionedArray dimensionedArray = toDimensionedArray();
        List<List<List<NodeResult>>> level0 = new ArrayList<>();
        for (int node = 0; node < dimensionedArray.nodes; node++) {
            List<List<NodeResult>> level1 = new ArrayList<>();
            for (int run = 0; run < dimensionedArray.runs; run++) {
                List<NodeResult> level2 = new ArrayList<>();
                for (int step = 0; step < dimensionedArray.steps; step++) {
                    level2.add(dimensionedArray.standardArray[run][step][node]);
                }
                level1.add(level2);
            }
            level0.add(level1);
        }
        return level0;
    }

    /**
     * Node-Step-Rum
     *
     * @return
     */
    public List<List<List<NodeResult>>> getNSROrder() {
        DimensionedArray dimensionedArray = toDimensionedArray();
        List<List<List<NodeResult>>> level0 = new ArrayList<>();
        for (int node = 0; node < dimensionedArray.nodes; node++) {
            List<List<NodeResult>> level1 = new ArrayList<>();
            for (int step = 0; step < dimensionedArray.steps; step++) {
                List<NodeResult> level2 = new ArrayList<>();
                for (int run = 0; run < dimensionedArray.runs; run++) {
                    level2.add(dimensionedArray.standardArray[run][step][node]);
                }
                level1.add(level2);
            }
            level0.add(level1);
        }
        return level0;
    }

    private DimensionedArray toDimensionedArray() {
        int runs = results.size();
        int steps = 0;
        int nodes = 0;
        for (RunResult runResult : results) {
            steps = Math.max(steps, runResult.getResults().size());
            for (StepResult stepResult : runResult.getResults()) {
                nodes = Math.max(nodes, stepResult.getNodeResults().size());
            }
        }

        NodeResult[][][] array = new NodeResult[runs][steps][nodes];
        for (int i = 0; i < runs; i++) {
            for (int j = 0; j < steps; j++) {
                for (int k = 0; k < nodes; k++) {
                    array[i][j][k] = null;
                }
            }
        }

        for (int run = 0; run < runs; run++) {
            RunResult runResult = results.get(run);
            for (int step = 0; step < steps; step++) {
                if (step < runResult.getResults().size()) {
                    StepResult stepResult = runResult.getResults().get(step);
                    for (int node = 0; node < nodes; node++) {
                        if (node < stepResult.getNodeResults().size()) {
                            array[run][step][node] = stepResult.getNodeResults().get(node);
                        }
                    }
                }
            }
        }

        return new DimensionedArray(runs, steps, nodes, array);
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("Report: max runs: ");
        sb.append(getMaxRunCount());
        sb.append(", max steps: ");
        sb.append(getMaxStepCount());
        sb.append(", max nodes: ");
        sb.append(getMaxNodeCount());
        sb.append('\n');
        int run = 0;
        for (RunResult runResult : getHierarchicalOrder()) {
            sb.append(runResult.toString());
            run++;
        }
        return sb.toString();
    }

}

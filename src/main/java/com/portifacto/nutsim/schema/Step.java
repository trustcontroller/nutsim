package com.portifacto.nutsim.schema;

import org.nd4j.shade.jackson.annotation.JsonAutoDetect;
import org.nd4j.shade.jackson.annotation.JsonIgnoreProperties;
import org.nd4j.shade.jackson.annotation.JsonInclude;

import java.util.ArrayList;
import java.util.List;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonIgnoreProperties(ignoreUnknown = true)
@JsonAutoDetect(getterVisibility = JsonAutoDetect.Visibility.NONE, setterVisibility = JsonAutoDetect.Visibility.NONE)
public class Step {

    public Integer time = null; // JSON
    public List<Node> nodes = null; // JSON
    public List<Opinion> opinions = new ArrayList<>();  // JSON

    public Step() {

    }

    public Integer getTime() {
        return time;
    }

    public Step setTime(int time) {
        this.time = time;
        return this;
    }

    public Step addNode(Node node) {
        nodes.add(node);
        return this;
    }

    public Step addOpinion(Opinion opinion) {
        opinions.add(opinion);
        return this;
    }

    public List<Node> getNodes() {
        return nodes;
    }

    public List<Opinion> getOpinions() {
        return opinions;

    }


}

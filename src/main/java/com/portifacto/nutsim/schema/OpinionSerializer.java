package com.portifacto.nutsim.schema;

import org.nd4j.shade.jackson.core.JsonGenerator;
import org.nd4j.shade.jackson.core.JsonProcessingException;
import org.nd4j.shade.jackson.databind.SerializerProvider;
import org.nd4j.shade.jackson.databind.ser.std.StdSerializer;

import java.io.IOException;

public class OpinionSerializer extends StdSerializer<Opinion> {
    public OpinionSerializer() {
        this(null);
    }

    public OpinionSerializer(Class<Opinion> vc) {
        super(vc);
    }

    @Override
    public void serialize(Opinion value, JsonGenerator jgen, SerializerProvider provider)
            throws IOException, JsonProcessingException {
        jgen.writeStartArray();
        if (value.getSource() == null) {
            jgen.writeNull();
        } else {
            jgen.writeNumber(value.getSource());
        }
        if (value.getDestination() == null) {
            jgen.writeNull();
        } else {
            jgen.writeNumber(value.getDestination());
        }
        if (value.getConfidence() == null) {
            jgen.writeNull();
        } else {
            jgen.writeNumber(value.getConfidence());
        }
        if (value.getValue() == null) {
            jgen.writeNull();
        } else {
            jgen.writeNumber(value.getValue());
        }
        jgen.writeEndArray();

    }

}


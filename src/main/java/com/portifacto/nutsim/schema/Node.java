package com.portifacto.nutsim.schema;

import com.portifacto.nutsim.parameters.Phenomenon;
import com.portifacto.nutsim.parameters.Role;
import com.portifacto.nutsim.parameters.RoleLabel;
import com.portifacto.nutsim.playbook.PlaybookException;
import org.nd4j.shade.jackson.annotation.JsonAutoDetect;
import org.nd4j.shade.jackson.annotation.JsonIgnoreProperties;
import org.nd4j.shade.jackson.annotation.JsonInclude;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonIgnoreProperties(ignoreUnknown = true)
@JsonAutoDetect(getterVisibility = JsonAutoDetect.Visibility.NONE, setterVisibility = JsonAutoDetect.Visibility.NONE)
public class Node {

    public Double measured = null;
    private double phenomenon;
    public RoleLabel role = null;

    private Role actualRole = null;

    public Node() {
    }

    public Node(Double measured, double phenomenon, RoleLabel role) {
        this.measured = measured;
        this.phenomenon = phenomenon;
        this.role = role;
    }

    public Node(Role role) {
        setRole(role);
    }

    public Node(Node alter) {
        this.measured = alter.measured;
        this.phenomenon = alter.phenomenon;
        this.actualRole = alter.actualRole;
        this.role = alter.role;
    }

    public Node(Node alter, double measured, double phenomenon) {
        this.measured = measured;
        this.phenomenon = phenomenon;
        this.actualRole = alter.actualRole;
        this.role = alter.role;
    }


    public void verify() throws PlaybookException {
        if (measured == null) {
            throw new PlaybookException("Expected must be defined");
        }
        if ((measured < 0.0) || (measured > 1.0)) {
            throw new PlaybookException("Expected must be between 0.0 and 1.0");
        }

        if (role == null) {
            role = RoleLabel.NONE;
        }

    }

    public Role getRole() {
        return actualRole;
    }

    public Node setRole(Role role) {
        actualRole = role;
        this.role = actualRole.getLabel();
        return this;
    }

    public double generateMeasurement(int step, Phenomenon phenomenon) {
        measured = actualRole.generateMeasurement(step, phenomenon);
        return measured;
    }

    public Double getMeasured() {
        return measured;
    }

    public double getPhenomenon() {
        return phenomenon;
    }


}

package com.portifacto.nutsim.schema;


import com.portifacto.nutsim.parameters.ParametersException;
import org.nd4j.shade.jackson.annotation.JsonIgnoreProperties;
import org.nd4j.shade.jackson.annotation.JsonInclude;
import org.nd4j.shade.jackson.databind.annotation.JsonDeserialize;
import org.nd4j.shade.jackson.databind.annotation.JsonSerialize;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonIgnoreProperties(ignoreUnknown = true)
@JsonDeserialize(using = OpinionDeserializer.class)
@JsonSerialize(using = OpinionSerializer.class)

public class Opinion implements Comparable<Opinion> {

    static private final double DEFAULT_CONFIDENCE = 1.0;

    public static class OpinionResult {
        private final int source;
        private final int destination;

        public OpinionResult(int source, int destination) {
            this.source = source;
            this.destination = destination;
        }

        public int getSource() {
            return source;
        }

        public int getDestination() {
            return destination;
        }
    }

    public Integer source = null; //JSON
    public Integer destination = null; // JSON
    public Double confidence = null; // JSON
    public Double value = null; // JSON

    public Opinion() {
    }

    public Opinion(Integer source, Integer destination, Double confidence, Double value) {
        this.source = source;
        this.destination = destination;
        this.confidence = confidence;
        this.value = value;
    }

    public Opinion(Integer source, Integer destination) {
        this(source, destination, null, null);
    }

    public Opinion(Integer source, Integer destination, Double confidence) {
        this(source, destination, confidence, null);
    }

    public Opinion(Opinion alter) {
        this(alter.source, alter.destination, alter.confidence, alter.value);
    }

    public Integer getSource() {
        return source;
    }

    public Integer getDestination() {
        return destination;
    }

    public Double getConfidence() {
        return confidence;
    }

    public Double getValue() {
        return value;
    }

    public OpinionResult verify(int suggestedSource, int suggestedDestination, int size) throws ParametersException {
        if (this.value == null) {
            throw new ParametersException("In opinion, at least its value must be provided");
        }
        // check values allowed
        if ((this.source != null) && ((this.source < 0) || (this.source >= size))) {
            throw new ParametersException("In opinion, 'source' is " + this.source +
                    ", but it must be between 0 and " + (size - 1));
        }
        if ((this.destination != null) && ((this.destination < 0) || (this.destination >= size))) {
            throw new ParametersException("In opinion, 'destination' is " + this.destination +
                    ", but it must be between 0 and " + (size - 1));
        }
        if ((this.confidence != null) && ((this.confidence < 0.0) || (this.confidence > 1.0))) {
            throw new ParametersException("In opinion, 'confidence' (source:" + this.source + ", to:" +
                    this.destination + ") must be between 0.0 and 1.0");
        }
        if ((value < 0.0) || (value > 1.0)) {
            throw new ParametersException("In opinion (source:" + this.source + ", destination: " +
                    this.destination + "), its value must be between 0.0 and 1.0");
        }
        // set cursor
        int defaultSource = suggestedSource;
        int defaultDestination = suggestedDestination;
        // do the substitution
        int currentSource;
        if (this.source != null) {
            currentSource = this.source;
            defaultSource = currentSource;
        } else {
            currentSource = defaultSource;
        }
        int currentDestination;
        if (this.destination != null) {
            currentDestination = this.destination;
            defaultDestination = currentDestination + 1;
        } else {
            currentDestination = defaultDestination;
            defaultDestination++;
        }
        double currentConfidence = this.confidence != null ? this.confidence : DEFAULT_CONFIDENCE;
        double currentValue = this.value;
        // wrap default from/to
        if (defaultDestination >= size) {
            defaultSource++;
            defaultDestination = 0;
            if (defaultSource >= size) {
                throw new ParametersException("Default location for opinions exceeds " + size + " x " + size);
            }
        }

        this.source = currentSource;
        this.destination = currentDestination;
        this.confidence = currentConfidence;
        this.value = currentValue;

        return new OpinionResult(defaultSource, defaultDestination);

    }


    @Override
    public boolean equals(Object alter) {
        if (!(alter instanceof Opinion)) {
            return false;
        }
        return this.source.equals(((Opinion) alter).source) &&
                this.destination.equals(((Opinion) alter).destination);
    }

    @Override
    public int compareTo(Opinion ego) {
        if (!this.source.equals(ego.source)) return this.source - ego.source;
        if (!this.destination.equals(ego.destination)) return this.destination - ego.destination;
        return 0;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("[ ");
        sb.append(source);
        sb.append(", ");
        sb.append(destination);
        sb.append(", ");
        sb.append(confidence == null ? "null" : String.format("%.3f", confidence));
        sb.append(", ");
        sb.append(value == null ? "null" : String.format("%.3f", value));
        sb.append(" ]");
        return sb.toString();
    }

}

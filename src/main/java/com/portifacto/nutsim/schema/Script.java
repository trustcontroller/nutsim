package com.portifacto.nutsim.schema;

import org.nd4j.shade.jackson.annotation.JsonAutoDetect;
import org.nd4j.shade.jackson.annotation.JsonIgnoreProperties;
import org.nd4j.shade.jackson.annotation.JsonInclude;
import org.nd4j.shade.jackson.databind.ObjectMapper;
import org.nd4j.shade.jackson.databind.SerializationFeature;

import java.util.ArrayList;
import java.util.List;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonIgnoreProperties(ignoreUnknown = true)
@JsonAutoDetect(getterVisibility = JsonAutoDetect.Visibility.NONE, setterVisibility = JsonAutoDetect.Visibility.NONE)
public class Script {

    private static final String TITLE = "default";

    public String title = null;

    public List<Step> timeline = new ArrayList<>(); // json

    public Script() {

    }

    public Script setTitle(String title) {
        this.title = title;
        return this;
    }

    public String getTitle() {
        return title == null ? TITLE : title;
    }

    public List<Step> getTimeline() {
        return timeline;
    }

    public Script addStep(Step step) {
        timeline.add(step);
        return this;
    }


    @Override
    public String toString() {
        try {
            ObjectMapper objectMapper = new ObjectMapper();
            objectMapper.enable(SerializationFeature.INDENT_OUTPUT);
            return objectMapper.writeValueAsString(this);
        } catch (Exception e) {
            return "Exception: " + e.toString();
        }
    }

}

package com.portifacto.nutsim.schema;

import org.nd4j.shade.jackson.core.JsonParser;
import org.nd4j.shade.jackson.core.JsonProcessingException;
import org.nd4j.shade.jackson.databind.DeserializationContext;
import org.nd4j.shade.jackson.databind.JsonNode;
import org.nd4j.shade.jackson.databind.deser.std.StdDeserializer;

import java.io.IOException;

public class OpinionDeserializer extends StdDeserializer<Opinion> {
    public OpinionDeserializer() {
        this(null);
    }

    public OpinionDeserializer(Class<?> vc) {
        super(vc);
    }

    @Override
    public Opinion deserialize(JsonParser jp, DeserializationContext ctxt)
            throws IOException, JsonProcessingException {
        JsonNode node = jp.getCodec().readTree(jp);
        if (node.isArray()) {
            // at least one, up to four elements, some of them nulls
            int size = node.size();
            if ((size < 1) || (size > 4)) {
                return new Opinion();
            }
            JsonNode node0 = null;
            JsonNode node1 = null;
            JsonNode node2 = null;
            JsonNode node3 = null;
            switch (size) {
                case 1:
                    node3 = node.get(0);
                    break;
                case 2:
                    node2 = node.get(0);
                    node3 = node.get(1);
                    break;
                case 3:
                    node1 = node.get(0);
                    node2 = node.get(1);
                    node3 = node.get(2);
                    break;
                case 4:
                    node0 = node.get(0);
                    node1 = node.get(1);
                    node2 = node.get(2);
                    node3 = node.get(3);
                    break;
            }
            // adjust nulls
            if ((node0 != null) && node0.isNull()) node0 = null;
            if ((node1 != null) && node1.isNull()) node1 = null;
            if ((node2 != null) && node2.isNull()) node2 = null;
            if ((node3 != null) && node3.isNull()) node3 = null;

            return new Opinion(node0 == null ? null : node0.asInt(),
                    node1 == null ? null : node1.asInt(),
                    node2 == null ? null : node2.asDouble(),
                    node3 == null ? null : node3.asDouble());
        } else if (node.isDouble()) {
            double tw = node.asDouble();
            return new Opinion(null, null, null, tw);

        } else {
            return new Opinion();
        }

    }
}

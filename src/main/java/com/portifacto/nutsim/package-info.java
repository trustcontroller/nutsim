package com.portifacto.nutsim;

/**
 * This package implements the simulator for the NUT model.
 * <p>
 * In order to runa simulation, it is necessary to create the Parameters object
 * that contains all the parameters, but nothing more. The Parameters object
 * can be created either from the JSON file, or programmatically.
 * <p>
 * From the Parameters object, the Playbook must be created. The Playbook
 * adds ot the Parameters what was not defined. Specifically, it generates
 * graphs and connections.
 * <p>
 * The Playbook is then passed to the Simulation, and it produces a Report.
 * The Report contains all the data points. It can be processed with the help
 * of Analytics.
 * <p>
 * <p>
 * Parameters (for generated graphs)   or Schema (for particular situation)
 * both create Playbook
 * <p>
 * <p>
 * Matching concepts:
 * Playbook - the unified description of the experiment to run,
 * consist of scripts
 * <p>
 * Script - the element of the playbook, describing one set of nodes
 * that will go through some steps, in each step having different edges;
 * when it comes to report, it is an equivalent of the run
 * <p>
 * Step - a single step in iteration that involves all nodes of one graph
 * (i.e. from one script); but having separate set of edges generated;
 * in the report its is also a step
 */
package com.portifacto.nutsim.simulation;


import com.portifacto.nutsim.common.Belief;
import com.portifacto.nutsim.parameters.ConfidenceType;
import com.portifacto.nutsim.parameters.FusionType;
import com.portifacto.nutsim.parameters.Parameters;
import com.portifacto.nutsim.parameters.Role;
import com.portifacto.nutsim.playbook.PlaybookNode;
import com.portifacto.nutsim.playbook.PlaybookOpinion;
import com.portifacto.nutsim.report.NodeResult;

import java.util.ArrayList;
import java.util.Deque;
import java.util.LinkedList;
import java.util.List;

public class SimulationNode {

    private int id; // simple identifier, runs from zero up

    private Deque<SimulationResult> history = new LinkedList<>(); // history of tw
    private double decay = 1.0;

    private Parameters parameters;

    private Role role = new Role();

    private Belief currentBelief;

    private double measured;
    private double phenomenon;

    public List<SimulationOpinion> getInboundEdges() {
        return inboundEdges;
    }

    private final List<SimulationOpinion> inboundEdges = new ArrayList<>();

    public SimulationNode(int id, Parameters parameters) {
        this.id = id;
        this.parameters = parameters;
        this.decay = parameters.getDecay();
    }

    /**
     * Invoked when the step contains the description of the node from the playbook
     * It is worth saving at least a role, as it may change
     */
    public void update(PlaybookNode node) {
        role = node.getRole();
        currentBelief=role.getJosang().getValue();
    }

    /**
     * Invoked for every node to clear its content in preparation for a new step
     */
    public void beginStep(int step) {
        inboundEdges.clear();
        phenomenon = parameters.getPhenomenon().getActualValue(step);
        measured = role.generateMeasurement(step, parameters.getPhenomenon());
    }

    /**
     * Invoked for the source of every opinion to make it complete
     */
    public SimulationOpinion calculateOpinion(int step,PlaybookOpinion playbookOpinion, SimulationNode destination) {
        double confidence;
        if (playbookOpinion.hasConfidence()) {
            confidence = playbookOpinion.getConfidence();
        } else {
            confidence = getConfidence(role.getConfidence(step).getType()); // mine, current
        }
        double value;
        if (playbookOpinion.hasValue()) {
            value = playbookOpinion.getValue();
        } else {
            value = parameters.getCalculation().calculateTrustworthiness(measured, destination.getMeasured());
        }
        double trustworthiness = getTrustworthiness(); // of the source, i.e. mine, current

        // belief is calculated using the discounting:
        // my opinion, with uncertainty copied, is discounted by my trustworthiness
        // i.e. I trust myself as much as others trust me
//        Belief calculated=Belief.ofEU(value,currentBelief.getUncertainty());
//        Belief calculated=Belief.ofEU(value,0); // I am certain of myself
        Belief calculated=Belief.of(value,1-value,0,value); // certain in myself
        Belief opinion=currentBelief.discounting(calculated);

        // note that the fixed confidence is not used here, as it is not necessary,
        // it derives from the trustworthiness
        return new SimulationOpinion(playbookOpinion.getSource(), playbookOpinion.getDestination(),
                confidence, value, trustworthiness,opinion);
    }

    /**
     * Aas the outbound node, invoked to collate complete opinions about itself
     */
    public void addOpinion(SimulationOpinion simulationOpinion) {
        inboundEdges.add(simulationOpinion);
    }

    /**
     * The core method to recalculate the state of the node
     */
    public void recalculate(int step) {

        double currentTrustworthiness=0; // as calculated in this cycle
        double currentConfidence=0; // this is the baseline confidence that the node has in itself
        double currentWeight=0; // weight of opinions behind the trustworthiness

        if (history.isEmpty()) {
            double trustworthiness = role.getTrustworthiness(0).getValue();
            double confidence = role.getConfidence(0).getValue();
            Belief belief=role.getJosang().getValue();
            SimulationResult simulationResult = new SimulationResult(id, role,
                    null, phenomenon, measured,
                    trustworthiness,
                    confidence,
                    0.0,
                    belief);
            history.addFirst(simulationResult);
            return;
        }

        if (!role.getTrustworthiness(step).getRecalculate()) {
            // get from the history
            currentTrustworthiness = history.getFirst().getTrustworthiness();
            currentWeight=history.getFirst().getWeight();
        } else {
            // collate from opinions in this round
            double trustworthinessNumerator = 0.0;
            double trustworthinessDenominator = 0.0;
            for (SimulationOpinion edge : getInboundEdges()) {
                switch(parameters.getFusion().getType()) {
                    case CONSENSUS:
                        trustworthinessNumerator += edge.getConfidence() * edge.getValue() * edge.getTrustworthiness();
                        trustworthinessDenominator += edge.getConfidence() * edge.getTrustworthiness();
                        break;
                    case TRANSITIVITY:
                        trustworthinessNumerator += edge.getConfidence() * edge.getValue() * edge.getTrustworthiness();
                        trustworthinessDenominator += 1;
                        break;
                    case REPUTATION:
                        trustworthinessNumerator += edge.getValue() * edge.getTrustworthiness();
                        trustworthinessDenominator += 1;
                        break;
                    case QUADRATIC:
                        trustworthinessNumerator += edge.getValue() * edge.getTrustworthiness()*edge.getTrustworthiness();
                        trustworthinessDenominator += 1;
                        break;
                    case WEIGHTED:
                        trustworthinessNumerator += edge.getConfidence() * edge.getValue() * edge.getTrustworthiness();
                        trustworthinessDenominator += edge.getConfidence() * edge.getTrustworthiness();
                        break;
                }
            }

            if(parameters.getFusion().getType()==FusionType.WEIGHTED) {
                trustworthinessNumerator+=
                        history.getFirst().getTrustworthiness()* history.getFirst().getWeight();
                trustworthinessDenominator+= history.getFirst().getWeight();
                double averageWeight= getInboundEdges().stream()
                        .mapToDouble(SimulationOpinion::getTrustworthiness)
                        .average().orElse(0.0);
                // apply EWMA to weight
                currentWeight=averageWeight*(1-decay) + history.getFirst().getWeight() *decay;
            }

            if (trustworthinessDenominator < 0.001) {
                currentTrustworthiness = 0.0;// the only valid case is zero over zero or no edges
            } else {
                currentTrustworthiness = trustworthinessNumerator / trustworthinessDenominator;
            }

            // apply EWMA to trustworthiness
            if(parameters.getFusion().getType()==FusionType.WEIGHTED) {

            } else {
                currentTrustworthiness = currentTrustworthiness * (1 - decay) + history.getFirst().getTrustworthiness() * decay;
            }

            // this is solely calculating beliefs
            if (!role.getJosang().getRecalculate()) {
                // get from the history
                currentBelief = history.getFirst().getBelief(); // do not decay
            } else {
                currentBelief=Belief.UNCERTAINTY;
                for (SimulationOpinion edge : getInboundEdges()) {
                    currentBelief=currentBelief.consensus(edge.getBelief());

                }
                currentBelief = currentBelief.consensus(history.getFirst().getBelief().decay(decay));
            }
        }

        if (!role.getConfidence(step).getRecalculate()) {
            currentConfidence = history.getFirst().getConfidence()*decay;
        } else {
            currentConfidence=0;
            double topTrustworthy=0;
            for (SimulationOpinion edge : getInboundEdges()) {
                if(edge.getTrustworthiness()>topTrustworthy &&
                edge.getTrustworthiness()>currentTrustworthiness) {
                    topTrustworthy=edge.getTrustworthiness();
                    currentConfidence=edge.getTrustworthiness()*edge.getValue();
                }
            }
            if(currentConfidence<history.getFirst().getConfidence()*decay) {
                currentConfidence=history.getFirst().getConfidence();
            }

        }

        SimulationResult simulationResult = new SimulationResult(id, role,
                new ArrayList<>(inboundEdges),
                phenomenon,
                measured,
                currentTrustworthiness,
                currentConfidence,
                currentWeight,
                currentBelief
        );

        history.addFirst(simulationResult);
    }

    /**
     * Clean-up after the step
     */
    public void endStep() {

    }

    public double getTrustworthiness() {
        if (history.isEmpty()) {
            return role.getTrustworthiness(0).getValue();
        }
        return history.getFirst().getTrustworthiness();
    }

    public double getConfidence() {
        return getConfidence(ConfidenceType.DEFINED);
    }

    public double getConfidence(ConfidenceType type) {
        switch(type) {
            case DEFINED:
                if (history.isEmpty()) {
                    return role.getConfidence(0).getValue();
                }
                return history.getFirst().getConfidence();
            case FROM_BELIEF:
                if (history.isEmpty()) {
                    return 0.0;
                }
                return 1.0 - history.getFirst().getBelief().getUncertainty();
        }
        return 0.0;
    }

    public double getWeight() {
        if (history.isEmpty()) {
            return 0.0;
        }
        return history.getFirst().getWeight();
    }

    public Belief getBelief() {
        if (history.isEmpty()) return Belief.UNCERTAINTY;
        return history.getFirst().getBelief();
    }

    public double getPhenomenon() {
        return phenomenon;
    }

    public double getMeasured() {
        return measured;
    }

    public NodeResult getResult() {
        NodeResult result = new NodeResult(id,
                getPhenomenon(), getMeasured(),
                getTrustworthiness(), getConfidence(), getWeight(),getBelief(),
                inboundEdges);
        return result;
    }

}

package com.portifacto.nutsim.simulation;

public class SimulationException extends Throwable {

    public SimulationException(String description) {
        super(description);
    }

    public SimulationException() {

    }
}

package com.portifacto.nutsim.simulation;

import com.portifacto.nutsim.common.Belief;
import com.portifacto.nutsim.parameters.Role;

import java.util.List;

public class SimulationResult {

    private final int id;
    private final Role role;
    private final List<SimulationOpinion> opinions;
    private final double phenomenon;
    private final double measured;
    private final double trustworthiness;
    private final double confidence;
    private final double weight;
    private final Belief belief;

    public SimulationResult(int id,
                            Role role,
                            List<SimulationOpinion> opinions,
                            double phenomenon,
                            double measured,
                            double trustworthiness,
                            double confidence,
                            double weight,
                            Belief belief) {
        this.id = id;
        this.phenomenon = phenomenon;
        this.measured = measured;
        this.trustworthiness = trustworthiness;
        this.role = role;
        this.opinions = opinions;
        this.confidence = confidence;
        this.weight=weight;
        this.belief = belief;

    }

    public double getTrustworthiness() {
        return trustworthiness;
    }

    public double getConfidence() {
        return confidence;
    }

    public Belief getBelief() {
        return belief;
    }

    public double getPhenomenon() {
        return phenomenon;
    }

    public double getMeasured() {
        return measured;
    }

    public double getWeight() {
        return weight;
    }
}

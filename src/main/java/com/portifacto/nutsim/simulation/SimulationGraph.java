package com.portifacto.nutsim.simulation;


import com.portifacto.nutsim.parameters.Parameters;
import com.portifacto.nutsim.playbook.PlaybookNode;
import com.portifacto.nutsim.playbook.PlaybookOpinion;
import com.portifacto.nutsim.playbook.PlaybookScript;
import com.portifacto.nutsim.playbook.PlaybookStep;
import com.portifacto.nutsim.report.StepResult;

import java.util.ArrayList;
import java.util.List;

public class SimulationGraph {

    private final PlaybookScript script;

    private final List<SimulationNode> nodes;

    private List<SimulationOpinion> opinions;

    public SimulationGraph(PlaybookScript script) {
        this.script = script;
        int size = script.getPlaybook().getSize();
        Parameters parameters = script.getPlaybook().getParameters();
        // create initial set of nodes, ids starting from zero
        nodes = new ArrayList<>(size);
        for (int i = 0; i < size; i++) nodes.add(new SimulationNode(i, parameters));
    }

    public StepResult step(PlaybookStep step) {

        // in the playbook, nodes are defined for every step, and their values can be different
        // even if they are identical, they are still defined
        // so that here we update only nodes with matching ids - i.e. the first
        // set from the beginning that is within the size and defined for the step
        for (int index = 0; index < nodes.size(); index++) {
            if (index < step.getNodes().size()) {
                PlaybookNode playbookNode = step.getNodes().get(index);
                nodes.get(index).update(playbookNode);
            }
        }

        // make sure all nodes are prepared for the current step
        // it also sets the measured value for each step
        for (SimulationNode simulationNode : nodes) {
            simulationNode.beginStep(step.getTime());
        }

        // for every opinion, ask the source node to supplement it with
        // confidence and value according to its algorithm
        opinions = new ArrayList<>();
        for (PlaybookOpinion opinion : step.getOpinions()) {
            int source = opinion.getSource();
            int destination = opinion.getDestination();
            opinions.add(nodes.get(source).calculateOpinion(step.getTime(),opinion, nodes.get(destination)));
        }

        // now distribute opinions to their destinations, creating their input lists
        for (SimulationOpinion opinion : opinions) {
            nodes.get(opinion.getDestination()).addOpinion(opinion);
        }

        // recalculate its own trustworthiness
        for (SimulationNode simulationNode : nodes) {
            simulationNode.recalculate(step.getTime());
        }

        // cleanup
        for (SimulationNode simulationNode : nodes) {
            simulationNode.endStep();
        }

        // collect results off nodes
        StepResult stepResult = new StepResult(step.getTime());
        for (SimulationNode simulationNode : nodes) {
            stepResult.appendResult(simulationNode.getResult());
        }

        return stepResult;

    }


}



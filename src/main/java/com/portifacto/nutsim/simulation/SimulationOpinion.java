package com.portifacto.nutsim.simulation;

import com.portifacto.nutsim.common.Belief;

public class SimulationOpinion {

    private final int source;
    private final int destination;
    private final double value;
    private final double confidence;
    private final double trustworthiness;
    private final Belief belief; // the same, but using beliefs

    public SimulationOpinion(int source, int destination, double confidence, double value,
                             double trustworthiness,Belief belief) {
        this.source = source;
        this.destination = destination;
        this.value = value;
        this.confidence = confidence;
        this.trustworthiness = trustworthiness;
        this.belief=belief;
    }

    public int getSource() {
        return source;
    }

    public int getDestination() {
        return destination;
    }

    public double getConfidence() {
        return confidence;
    }

    public double getValue() {
        return value;
    }

    public double getTrustworthiness() {
        return trustworthiness;
    }

    public Belief getBelief() {
        return belief;
    }


}

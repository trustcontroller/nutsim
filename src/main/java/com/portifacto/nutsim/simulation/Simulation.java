package com.portifacto.nutsim.simulation;

import com.portifacto.nutsim.playbook.Playbook;
import com.portifacto.nutsim.playbook.PlaybookScript;
import com.portifacto.nutsim.playbook.PlaybookStep;
import com.portifacto.nutsim.report.Report;
import com.portifacto.nutsim.report.RunResult;
import com.portifacto.nutsim.report.StepResult;

import java.util.List;

/**
 * Basic concepts:
 * Single run is the replay of a particular configuration of a graph
 * with edges changing from step to step.
 */
public class Simulation implements ISimulation {

    public Simulation() {
    }

    public Report run(Playbook playbook) {

        Report report = new Report(playbook);

        List<PlaybookScript> scripts = playbook.getScripts();
        for (PlaybookScript script : scripts) {

            RunResult runResult = new RunResult(script);

            SimulationGraph simulationGraph = new SimulationGraph(script);

            for (PlaybookStep step : script.getTimeline()) {
                StepResult stepResult = simulationGraph.step(step);

                runResult.appendStep(stepResult);

            }
            report.appendRun(runResult);
        }


        return report;
    }

}

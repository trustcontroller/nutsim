package com.portifacto.nutsim.simulation;

import com.portifacto.nutsim.playbook.Playbook;
import com.portifacto.nutsim.report.Report;

public interface ISimulation {

    public Report run(Playbook playbook) throws SimulationException;
}

package com.portifacto.nutsim.analytics;

import com.portifacto.nutsim.common.Belief;
import com.portifacto.nutsim.parameters.RoleLabel;
import com.portifacto.nutsim.report.NodeResult;
import com.portifacto.nutsim.report.Report;
import com.portifacto.nutsim.report.RunResult;
import com.portifacto.nutsim.report.StepResult;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.stream.IntStream;

public class Analytics {

    private final static double EPSILON = 0.001;
    private final static double DEFAULT = 0.5;

    private final Report report;

    public Analytics(Report report) {
        this.report = report;
    }

    public Report getReport() {
        return report;
    }

    public int getRunCount() {
        return report.getMaxRunCount();
    }

    public int getStepCount() {
        return report.getMaxStepCount();
    }

    public int getNodeCount() {
        return report.getMaxNodeCount();
    }

    public List<ConfusionMatrix> confusionMatrixPerStep(IClassifier classifier) {

        int steps = report.getMaxStepCount();

        List<ConfusionMatrix> outcome = new ArrayList<>();
        IntStream.range(0, steps).forEach(x -> outcome.add(new ConfusionMatrix(report.getPlaybook(), classifier)));

        List<RunResult> scriptResultList = report.getHierarchicalOrder();
        for (RunResult scriptResult : scriptResultList) {
            List<StepResult> stepResultList = scriptResult.getResults();
            for (int step = 0; step < stepResultList.size(); step++) {
                StepResult stepResult = stepResultList.get(step);
                for (NodeResult result : stepResult.getNodeResults()) {
                    outcome.get(step).classify(result);
                }
            }

        }
        return outcome;
    }

    public double misClassifiedTotalInAllButFirstStep(List<ConfusionMatrix> confusionMatrixList) {
        List<ConfusionMatrix> shortList = new ArrayList<>(confusionMatrixList);
        shortList.remove(0);
        return shortList
                .stream()
                .mapToDouble(x -> x
                        .getMisclassified())
                .average()
                .orElse(0.0);
    }

    public double misClassifiedTasFInAllButFirstStep(List<ConfusionMatrix> confusionMatrixList) {
        List<ConfusionMatrix> shortList = new ArrayList<>(confusionMatrixList);
        shortList.remove(0);
        return shortList
                .stream()
                .mapToDouble(x -> x
                        .getFN())
                .average()
                .orElse(0.0);
    }

    public double misClassifiedFasTInAllButFirstStep(List<ConfusionMatrix> confusionMatrixList) {
        List<ConfusionMatrix> shortList = new ArrayList<>(confusionMatrixList);
        shortList.remove(0);
        return shortList
                .stream()
                .mapToDouble(x -> x
                        .getFP())
                .average()
                .orElse(0.0);
    }

    /**
     * Calculates the theoretical response of the network to the situation where all
     * nodes are exposed to unit stimulus, and trustworthy report it correctly
     * while faulty report zero instead.
     * Describes the theoretical quality of the whole network - i.e.
     * how good is the description of the physical phenomenon despite some nodes being
     * faulty.
     * The outcome is for a step; for each step it is calculated for all nodes in the given run, then averaged
     * for all runs
     *
     * @return
     */
    public List<Double> averageUnitResponsePerStepStandard() {
        List<List<List<NodeResult>>> array = report.getSRNOrder();

        List<Double> result = new ArrayList<>();

        for (List<List<NodeResult>> runList : array) {
            double sum = 0;
            int count = 0;
            for (List<NodeResult> nodeList : runList) {
                sum += LossFunctions.standard(nodeList).unitResponse();
                count++;
            }
            result.add(sum / count);
        }
        return result;
    }



    public List<Double> averageExpectationPerStepBelief() {

        List<List<List<NodeResult>>> array = report.getSRNOrder();
        List<Double> result = new ArrayList<>();

        for (List<List<NodeResult>> runList : array) {
            double sum = 0;
            int count = 0;
            for (List<NodeResult> nodeList : runList) {
                for (NodeResult r : nodeList) {
                    sum += r.getBelief().getExpectation();
                    count++;
                }
            }
            result.add(count == 0 ? Double.NaN : sum / count);
        }
        return result;
    }

    public List<Double> averageTrustworthinessPerStepStandard() {

        List<List<List<NodeResult>>> array = report.getSRNOrder();
        List<Double> result = new ArrayList<>();

        for (List<List<NodeResult>> runList : array) {
            double sum = 0;
            int count = 0;
            for (List<NodeResult> nodeList : runList) {
                for (NodeResult r : nodeList) {
                    sum += r.getTrustworthiness();
                    count++;
                }
            }
            result.add(count == 0 ? Double.NaN : sum / count);
        }
        return result;
    }


//        int steps = report.getMaxStepCount();
//        int runs = report.getMaxRunCount();
//
//        List<Double> outcome = new ArrayList<>();
//
//        if (steps == 0) return outcome;
//        IntStream.range(0, steps).forEach(x -> outcome.add(0.0));
//
//        List<RunResult> scriptResultList = report.getHierarchicalOrder();
//        for (RunResult scriptResult : scriptResultList) {
//            List<StepResult> stepResultList = scriptResult.getResults();
//            for (int step = 0; step < stepResultList.size(); step++) {
//                StepResult stepResult = stepResultList.get(step);
//                for (NodeResult result : stepResult.getNodeResults()) {
//                    outcome.set(step, outcome.get(step) + result.getTrustworthiness());
//                }
//            }
//
//        }
//        for (int i = 0; i < outcome.size(); i++) {
//            outcome.set(i, outcome.get(i) / (double) runs);
//        }
//
//        return outcome;


    public List<Double> averageUnitResponsePerStepPlausibility() {

        List<List<List<NodeResult>>> array = report.getSRNOrder();
        List<Double> result = new ArrayList<>();

        for (List<List<NodeResult>> runList : array) {
            double sum = 0;
            int count = 0;
            for (List<NodeResult> nodeList : runList) {
                sum += LossFunctions.plausibility(nodeList).unitResponse();
                count++;
            }
            result.add(sum / count);
        }
        return result;
    }

    public List<Double> averageUnitResponsePerStepBelief() {

        List<List<List<NodeResult>>> array = report.getSRNOrder();
        List<Double> result = new ArrayList<>();

        for (List<List<NodeResult>> runList : array) {
            double sum = 0;
            int count = 0;
            for (List<NodeResult> nodeList : runList) {
                sum += LossFunctions.belief(nodeList).unitResponse();
                count++;
            }
            result.add(sum / count);
        }
        return result;
    }

    public static double averageUnitResponseInAllButFirstStep(List<Double> unitResponseList) {
        List<Double> thisResponseList = new ArrayList<>(unitResponseList);
        thisResponseList.remove(0);
        return thisResponseList.stream().mapToDouble(x -> x).average().orElse(0.0);
    }

    public static double averageUnitResponseInLastStep(List<Double> unitResponseList) {
        return unitResponseList.isEmpty() ? Double.NaN : unitResponseList.get(unitResponseList.size() - 1);
    }

    // traces node average trustworthiness for all runs, in each step (used for flipping)
    public List<Double> nodeStandardTraceAverage(int node) {

        int steps = report.getMaxStepCount();

        List<Double> outcome = new ArrayList<>();
        List<Integer> count = new ArrayList<>();

        if (steps == 0) return outcome;
        IntStream.range(0, steps).forEach(x -> {
            outcome.add(0.0);
            count.add(0);
        });

        List<RunResult> scriptResultList = report.getHierarchicalOrder();
        for (RunResult scriptResult : scriptResultList) {
            List<StepResult> stepResultList = scriptResult.getResults();
            for (int step = 0; step < stepResultList.size(); step++) {
                StepResult stepResult = stepResultList.get(step);
                List<NodeResult> nodeResultList = stepResult.getNodeResults();
                if (node < nodeResultList.size()) {
                    NodeResult result = nodeResultList.get(node);
                    outcome.set(step, outcome.get(step) + result.getTrustworthiness());
                    count.set(step, count.get(step) + 1);
                }
            }

        }
        for (int i = 0; i < outcome.size(); i++) {
            outcome.set(i, outcome.get(i) / (double) count.get(i));
        }

        return outcome;

    }

    // traces node average expectation, out of beliefs for all runs, in each step
    public List<Double> nodeExpectationTraceAverage(int node) {

        int steps = report.getMaxStepCount();

        List<Double> outcome = new ArrayList<>();
        List<Integer> count = new ArrayList<>();

        if (steps == 0) return outcome;
        IntStream.range(0, steps).forEach(x -> {
            outcome.add(0.0);
            count.add(0);
        });

        List<RunResult> scriptResultList = report.getHierarchicalOrder();
        for (RunResult scriptResult : scriptResultList) {
            List<StepResult> stepResultList = scriptResult.getResults();
            for (int step = 0; step < stepResultList.size(); step++) {
                StepResult stepResult = stepResultList.get(step);
                List<NodeResult> nodeResultList = stepResult.getNodeResults();
                if (node < nodeResultList.size()) {
                    NodeResult result = nodeResultList.get(node);
                    outcome.set(step, outcome.get(step) + result.getBelief().getExpectation());
                    count.set(step, count.get(step) + 1);
                }
            }
        }
        for (int i = 0; i < outcome.size(); i++) {
            outcome.set(i, outcome.get(i) / (double) count.get(i));
        }

        return outcome;
    }

    public List<Double> nodePlausibilityTraceAverage(int node) {

        int steps = report.getMaxStepCount();

        List<Double> outcome = new ArrayList<>();
        List<Integer> count = new ArrayList<>();

        if (steps == 0) return outcome;
        IntStream.range(0, steps).forEach(x -> {
            outcome.add(0.0);
            count.add(0);
        });

        List<RunResult> scriptResultList = report.getHierarchicalOrder();
        for (RunResult scriptResult : scriptResultList) {
            List<StepResult> stepResultList = scriptResult.getResults();
            for (int step = 0; step < stepResultList.size(); step++) {
                StepResult stepResult = stepResultList.get(step);
                List<NodeResult> nodeResultList = stepResult.getNodeResults();
                if (node < nodeResultList.size()) {
                    NodeResult result = nodeResultList.get(node);
                    outcome.set(step, outcome.get(step) + result.getBelief().getBelief()+(1-result.getBelief().getUncertainty()));
                    count.set(step, count.get(step) + 1);
                }
            }
        }
        for (int i = 0; i < outcome.size(); i++) {
            outcome.set(i, outcome.get(i) / (double) count.get(i));
        }

        return outcome;
    }

    // traces node average trustworthiness for all runs, in each step
    public List<Double> nodeConfidenceTraceAverage(int node) {

        int steps = report.getMaxStepCount();

        List<Double> outcome = new ArrayList<>();
        List<Integer> count = new ArrayList<>();

        if (steps == 0) return outcome;
        IntStream.range(0, steps).forEach(x -> {
            outcome.add(0.0);
            count.add(0);
        });

        List<RunResult> scriptResultList = report.getHierarchicalOrder();
        for (RunResult scriptResult : scriptResultList) {
            List<StepResult> stepResultList = scriptResult.getResults();
            for (int step = 0; step < stepResultList.size(); step++) {
                StepResult stepResult = stepResultList.get(step);
                List<NodeResult> nodeResultList = stepResult.getNodeResults();
                if (node < nodeResultList.size()) {
                    NodeResult result = nodeResultList.get(node);
                    outcome.set(step, outcome.get(step) + result.getConfidence());
                    count.set(step, count.get(step) + 1);
                }
            }

        }
        for (int i = 0; i < outcome.size(); i++) {
            outcome.set(i, outcome.get(i) / (double) count.get(i));
        }

        return outcome;

    }

    public List<Belief> nodeBeliefTraceAverage(int node) {

        // the 'average belief' is not a consensus, but a synthetic belief consisting of
        // normalized d, b, u

        int steps = report.getMaxStepCount();

        List<Belief> outcome = new ArrayList<>();
        List<Double> d=new ArrayList<>();
        List<Double> b=new ArrayList<>();
        List<Double> u=new ArrayList<>();
        List<Integer> count=new ArrayList<>();

        if (steps == 0) return outcome;
        IntStream.range(0, steps).forEach(x -> {
            d.add(0.0);
            b.add(0.0);
            u.add(0.0);
            count.add(0);
        });

        List<RunResult> scriptResultList = report.getHierarchicalOrder();
        for (RunResult scriptResult : scriptResultList) {
            List<StepResult> stepResultList = scriptResult.getResults();
            for (int step = 0; step < stepResultList.size(); step++) {
                StepResult stepResult = stepResultList.get(step);
                List<NodeResult> nodeResultList = stepResult.getNodeResults();
                if (node < nodeResultList.size()) {
                    NodeResult result = nodeResultList.get(node);
                    d.set(step,d.get(step)+result.getBelief().getDisbelief());
                    b.set(step,b.get(step)+result.getBelief().getBelief());
                    u.set(step,u.get(step)+result.getBelief().getUncertainty());
                    count.set(step,count.get(step)+1);
                }
            }

        }

        IntStream.range(0, steps).forEach(x -> {
            // normalize to one

            outcome.add(Belief.of(b.get(x),d.get(x),u.get(x)));

        });

        return outcome;

    }

    public List<Double> averageErrorPerStep() {
        int steps = report.getMaxStepCount();

        List<Double> outcome = new ArrayList<>();
        List<Integer> count = new ArrayList<>();

        if (steps == 0) return outcome;
        IntStream.range(0, steps).forEach(x -> {
            outcome.add(0.0);
            count.add(0);
        });

        List<RunResult> scriptResultList = report.getHierarchicalOrder();
        for (RunResult scriptResult : scriptResultList) {
            List<StepResult> stepResultList = scriptResult.getResults();
            for (int step = 0; step < stepResultList.size(); step++) {
                StepResult stepResult = stepResultList.get(step);
                List<NodeResult> nodeResultList = stepResult.getNodeResults();
                for (int node = 0; node < nodeResultList.size(); node++) {
                    if (node < nodeResultList.size()) {
                        NodeResult result = nodeResultList.get(node);
//                        outcome.set(step, outcome.get(step) + (result.getEstimationError()*result.getEstimationError()));
                        count.set(step, count.get(step) + 1);
                    }
                }
            }

        }
        for (int i = 0; i < outcome.size(); i++) {
            outcome.set(i, outcome.get(i) / (double) count.get(i));
        }

        return outcome;

    }
}

package com.portifacto.nutsim.analytics;

public enum ClassifierLabel {
    GOOD,
    BAD
}

package com.portifacto.nutsim.analytics;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.util.List;

public class CsvWriter {

    private BufferedWriter bw = null;

    public CsvWriter(String fileName) {
        try {
            bw = new BufferedWriter(new FileWriter(fileName));
        } catch (Exception e) {

        }

    }

    public void write(Object... list) {
        try {
            StringBuilder sb = new StringBuilder();
            for (Object o : list) {
                if (o instanceof String) {
                    sb.append('\"');
                    sb.append((String) o);
                    sb.append('\"');
                } else if (o instanceof Integer) {
                    sb.append((Integer) o);
                } else if (o instanceof Double) {
                    sb.append(String.format("%.5f", (Double) o));
                }
                sb.append(',');

            }
            sb.append('\n');
            if (bw != null) {
                bw.write(sb.toString());
                bw.flush();
            }
        } catch (Exception e) {

        }
    }

    public void write(List<Double> list) {
        write(null,list);
     }

    public void write(String prefix,List<Double> list) {
        try {
            StringBuilder sb = new StringBuilder();
            if(prefix!=null) {
                sb.append('\"');
                sb.append(prefix);
                sb.append("\",");
            }
            for (Double d : list) {
                sb.append(String.format("%.5f", d));
                sb.append(',');
            }
            sb.append('\n');
            if (bw != null) {
                bw.write(sb.toString());
                bw.flush();
            }
        } catch (Exception e) {

        }
    }
    public void writeIntegerList(List<Integer> list) {
        try {
            StringBuilder sb = new StringBuilder();
            for (Integer d : list) {
                sb.append(String.format("%d", d));
                sb.append(',');
            }
            sb.append('\n');
            if (bw != null) {
                bw.write(sb.toString());
                bw.flush();
            }
        } catch (Exception e) {

        }
    }

    public void flush() {
        if (bw != null) {
            try {
                bw.flush();
            } catch (Exception e) {

            }
        }

    }

    public void close() {
        if (bw != null) {
            try {
                bw.flush();
                bw.close();
            } catch (Exception e) {

            } finally {
                bw = null;
            }
        }
    }

}

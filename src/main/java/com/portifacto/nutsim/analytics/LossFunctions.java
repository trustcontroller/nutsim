package com.portifacto.nutsim.analytics;

import com.portifacto.nutsim.report.NodeResult;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class LossFunctions {

    private static class DataPoint {
        private final double actual;
        private final double expected;

        public DataPoint(double expected, double actual) {
            this.actual = actual;
            this.expected = expected;
        }

        public double getActual() {
            return actual;
        }

        public double getExpected() {
            return expected;
        }
    }

    private final List<DataPoint> dataPoints;

    static public LossFunctions standard(List<NodeResult> nodes) {
        List<DataPoint> dataPoints = new ArrayList<>();
        for (NodeResult nodeResult : nodes) {
            if (nodeResult != null) {
                dataPoints.add(new DataPoint(nodeResult.getTrustworthiness(), nodeResult.getMeasured()));
            }
        }
        return new LossFunctions(dataPoints);
    }

    static public LossFunctions belief(List<NodeResult> nodes) {
        List<DataPoint> dataPoints = new ArrayList<>();
        for (NodeResult nodeResult : nodes) {
            if (nodeResult != null) {
                dataPoints.add(new DataPoint(nodeResult.getBelief().getExpectation(), nodeResult.getMeasured()));
            }
        }
        return new LossFunctions(dataPoints);

    }

    static public LossFunctions plausibility(List<NodeResult> nodes) {
        List<DataPoint> dataPoints = new ArrayList<>();
        for (NodeResult nodeResult : nodes) {
            if (nodeResult != null) {
                dataPoints.add(new DataPoint(
                        nodeResult.getBelief().getBelief()*(1-nodeResult.getBelief().getUncertainty()),
                        nodeResult.getMeasured()));
            }
        }
        return new LossFunctions(dataPoints);

    }


    public LossFunctions(List<DataPoint> dataPoints) {
        this.dataPoints = dataPoints;
    }

    public LossFunctions(List<Double> actual, List<Double> expected) {
        dataPoints = generateDataPoints(actual, expected);
    }

    public LossFunctions(List<Double> actual, double expected) {
        dataPoints = generateDataPoints(actual, expected);
    }

    public double unitResponse() {
        if (dataPoints == null) return Double.NaN;
        if (dataPoints.isEmpty()) return Double.NaN;
        double sumActual = dataPoints
                .stream()
                .mapToDouble(DataPoint::getExpected)
                .sum();
        return dataPoints
                .stream()
                .mapToDouble(dataPoint -> dataPoint.getExpected() * dataPoint.getActual() / sumActual)
                .sum();
    }

    public double meanSquaredError() {
        return dataPoints
                .stream()
                .mapToDouble(dataPoint -> dataPoint.getActual() - dataPoint.getExpected())
                .boxed()
                .mapToDouble(x -> x * x)
                .average().getAsDouble();
    }

    public double crossEntropy() {
        return -dataPoints
                .stream()
                .mapToDouble(point -> point.getActual() * Math.log(point.getExpected()) / Math.log(2.0))
                .sum();
    }


    private List<DataPoint> generateDataPoints(List<Double> list, double reference) {
        return list
                .stream()
                .mapToDouble(x -> x)
                .mapToObj(x -> new DataPoint(x, reference))
                .collect(Collectors.toList());
    }

    private List<DataPoint> generateDataPoints(List<Double> list, List<Double> reference) {
        if (list.size() != reference.size()) {
            return null;
        }
        List<DataPoint> result = new ArrayList<>();
        for (int i = 0; i < list.size(); i++) {
            result.add(new DataPoint(list.get(i), reference.get(i)));
        }
        return result;
    }
}

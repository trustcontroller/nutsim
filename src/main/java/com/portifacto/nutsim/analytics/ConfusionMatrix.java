package com.portifacto.nutsim.analytics;

import com.portifacto.nutsim.playbook.Playbook;
import com.portifacto.nutsim.report.NodeResult;

public class ConfusionMatrix {

    private final IClassifier classifier;

    private int badAsBad = 0; // failed assessed as failed PN
    private int goodAsBad = 0; // trustworthy assessed as failed FN
    private int badAsGood = 0; // failed assessed as trustworthy FP
    private int goodAsGood = 0; // trustworthy assessed as trustworthy PP

    private int count = 0;

    private final Playbook playbook;


    public ConfusionMatrix(Playbook playbook, IClassifier classifier) {
        this.classifier = classifier;
        this.playbook = playbook;

    }

    public void classify(NodeResult result) {
        // rules for classification (good/bad)
        // actual: trustworthiness, through selected classifier (>0.5 -> good)
        // bad node is the one whose current reading is off the actual phenomenon
        ClassifierLabel actual = classifier.classify(result.getTrustworthiness());
        double phenomenonValue = playbook.getParameters().getPhenomenon().getValue();
        double actualValue = result.getMeasured();
        double nodeTrustworthiness = playbook
                .getParameters().getCalculation().calculateTrustworthiness(actualValue, phenomenonValue);
        ClassifierLabel expected = classifier.classify(nodeTrustworthiness);

        if (expected == ClassifierLabel.GOOD) {
            if (actual == ClassifierLabel.GOOD) {
                goodAsGood++;
            } else if (actual == ClassifierLabel.BAD) {
                goodAsBad++;
            }
        } else if (expected == ClassifierLabel.BAD) {
            if (actual == ClassifierLabel.GOOD) {
                badAsGood++;
            } else if (actual == ClassifierLabel.BAD) {
                badAsBad++;
            }
        }
        count++;
    }

    public double getTN() {
        return badAsBad / (double) count;
    }

    public double getFN() {
        return goodAsBad / (double) count;
    }

    public double getFP() {
        return badAsGood / (double) count;
    }

    public double getTP() {
        return (double) goodAsGood / (double) count;
    }

    public double getMisclassified() {
        return (double) (goodAsBad + badAsGood) / (double) count;
    }

    public double getPrecision() {
        return getTP() / (getTP() + getFP());
    }

    public double getRecall() {
        return getTP() / (getTP() + getTN());
    }

    public double getF1() {
        return 2.0 * (getPrecision() * getRecall()) / (getPrecision() + getRecall());
    }

    public double getCount() {
        return count;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(" TP(TaT): ");
        sb.append(String.format("%.3f", getTP()));
        sb.append(", FN(TaF): ");
        sb.append(String.format("%.3f", getFN()));
        sb.append(", FP(FaT): ");
        sb.append(String.format("%.3f", getFP()));
        sb.append(", TN(FaF): ");
        sb.append(String.format("%.3f", getTN()));
        sb.append(", total: ");
        sb.append(count);
        sb.append(", TaT: ");
        sb.append(goodAsGood);
        sb.append(", TaF: ");
        sb.append(goodAsBad);
        sb.append(", FaT: ");
        sb.append(badAsGood);
        sb.append(", FaF: ");
        sb.append(badAsBad);
        return sb.toString();
    }
}

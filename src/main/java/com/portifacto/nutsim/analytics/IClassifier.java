package com.portifacto.nutsim.analytics;

public interface IClassifier {
    public ClassifierLabel classify(double trustworthiness);
}

### Parameters

The parameter object, or an equivalent parameter file, is used to create 
the playbook that contains generated graphs and opinions. The generation,
as well as the later simulation is controlled by setting some values of
parameters.

title 

runs 

steps

decay

graphType

constraints

**calculation** Determines how the calculation of a trustworthiness is performed, 
off the measurements of the node that provides opinion and the other one 
of which the opinion is provided.

size

**density** Sets the graph density. The density is the average number of
inbound (or outbound) edges (opinions) per node. The set density is guaranteed
for every generated graph, but within the available precision.
If the density is not an integral, then the actual density may be slightly higher.
Consider e.g. 10-nodes graph of a density of 1.23, i.e. the graph that
should contain 12.3 edges. As it is not possible, then the number of edges
will be rounded up to 13.
The default density is 1.0.

phenomenon

**role** Defines the role for a fraction of nodes. There can be several roles
defined in the same set of parameters.





